"""
Django settings for eseditor project.

Generated by 'django-admin startproject' using Django 1.9.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

LOGIN_URL = '/accounts/login/'

LOGIN_REDIRECT_URL = '/'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'i-!@_#30xjm3f#v=zz=0df6sz6u*-q32h+^t#%4#&k&)vqe54*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']

if DEBUG:
    SITE_URL = 'http://localhost:8000'
else:
    SITE_URL = 'https://ec2-54-254-187-31.ap-southeast-1.compute.amazonaws.com'

DEFAULT_FROM_EMAIL_NAME = 'Education Studio'
DEFAULT_FROM_EMAIL = 'EducationStudio<info@educationstudio.com>'
DEFAULT_CONTACT_EMAIL = 'info@educationstudio.com'

EMAIL_USE_TLS = True
EMAIL_HOST = ''
EMAIL_PORT = 25
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''

SENDGRID_API_KEY = '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'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'star_ratings',
    'password_reset',
    'accounts',
    'editors',
    'students',
    'message',
    'solo'
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'eseditor.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR , 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                "django.core.context_processors.i18n",
                "django.core.context_processors.media",
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.request',
                'eseditor.context_processor.user_info',
                'message.context_processors.get_message_counter'
            ],
        },
    },
]

WSGI_APPLICATION = 'eseditor.wsgi.application'

if DEBUG:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'educationstudioprod',
            'USER': 'EDUCATIONSTUDIO',
            'PASSWORD': 'education123studio',
            'HOST': 'educationstudioprod.cofqeph3h9ny.ap-southeast-1.rds.amazonaws.com',
            'PORT': 5432,
        }
    }

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, "media")

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

MEDIA_ROOT = os.path.join('media/')

MEDIA_URL = '/media/'

STATIC_URL = '/static/'

if DEBUG:
    STATICFILES_DIRS = [
        os.path.join(BASE_DIR, "static"),
    ]
else:
    STATIC_ROOT = 'static'

    STATICFILES_DIRS = [
        '/var/www/static/',
    ]

X_FRAME_OPTIONS = 'DENY'

ENCRYPTION_SECRET_KEY = 'educationstudio'

STAR_RATINGS_RERATE = False
STAR_RATINGS_ANONYMOUS = True

# Stripe Settings
#if DEBUG:
STRIPE_SECRET_KEY = 'sk_test_VlXTsJoEB3dF2qr7ywkm8fvB'
STRIPE_PUBLIC_KEY = 'pk_test_KfBF3zrhcRLHn70qWVBbmZxi'
STRIPE_CLIENT_ID = 'ca_ACsMgT12WfcjcWVUNYci9bQSwrBgLbhm'
#else:
#STRIPE_SECRET_KEY = 'sk_live_K6CficX6SeoFpjpXFRpKhlXb'
#STRIPE_PUBLIC_KEY = 'pk_live_DwcC7cwO3dQpR1Z5unJGNwJf'
#STRIPE_CLIENT_ID = 'ca_8Garu2hZT6V9QsxUpLvb0B0j5MbVq3ak'

STRIPE_AUTHORIZE_URL = 'https://connect.stripe.com/oauth/authorize'
STRIPE_TOKEN_URL = 'https://connect.stripe.com/oauth/token'

# EducationStudio service changes in percentage
PLATFORM_FEE = 5
PLATFORM_CURRENCY = 'aud'
PLATFORM_CURRENCY_DISPLAY = 'AUD'

#Quiz Settings

#quiz time can be increased or decreased by changing the value of minutes
QUIZ_TIME = 30

#no of questions
NO_EASY_QUESTION = 5
NO_MEDIUM_QUESTION = 10
NO_HARD_QUESTION = 5

