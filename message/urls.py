from django.conf.urls import url


urlpatterns = [
    url(r'^$', 'message.views.message', name='message'),
    url(r'^details/(?P<msg_id>[0-9]+)/$', 'message.views.message_details', name='message_details'),

]    