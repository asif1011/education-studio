import base64
from base64 import b64decode

from Crypto.Cipher import AES


class AESCipher(object):
    """
    encrypt/decrypt message with secret_key.
    """
    IV_STRING = 'eds_iv_string'
    BLOCK_SIZE = 16

    def __init__(self, secret_key):
        self.secret_key = secret_key
        self.aes = AES.new(self._pad(secret_key), AES.MODE_CBC, self._pad(self.IV_STRING))

    def _pad(self, raw_str):
        """
        util function to change string length
        passphrase MUST be 16, 24 or 32 bytes long
        """
        block_size = self.BLOCK_SIZE
        extra_len = (block_size - len(raw_str) % block_size)
        pad_str = raw_str + extra_len * chr(extra_len)
        return pad_str

    @staticmethod
    def _unpad(pad_s):
        """
        reverse of `pad` function
        """
        raw_s = pad_s[:-ord(pad_s[len(pad_s)-1:])]
        return raw_s

    def encrypt(self, message):
        """
        encypt message with secret_key.
        """
        message = self._pad(message)
        ciphertext = self.aes.encrypt(message)
        encypted_message = base64.b64encode(ciphertext)
        return encypted_message

    def decrypt(self, encypted_message):
        """
        decypt message with secret_key.
        """
        encypted_msg_base64 = base64.b64decode(encypted_message)
        ciphertext = self.aes.decrypt(encypted_msg_base64)
        message = self._unpad(ciphertext)
        return message


def encrypt_aes(message, secret_key):
    """
    Wrapper of AESCipher's encrypt function.
    If no secret_key, return raw message directly
    """

    cipher = AESCipher(secret_key)
    encrypted_msg = cipher.encrypt(message)
    return encrypted_msg


def decrypt_aes(message, secret_key):
    """
    Wrapper of AESCipher's decrypt function.
    If no secret_key, return message directly
    """

    cipher = AESCipher(secret_key)
    raw_msg = cipher.decrypt(message)
    return raw_msg
