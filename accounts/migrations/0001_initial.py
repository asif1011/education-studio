# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-10-27 13:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PasswordResetAuth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=75)),
                ('token', models.CharField(max_length=11)),
                ('is_expired', models.BooleanField(default=False)),
            ],
        ),
    ]
