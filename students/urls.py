from django.conf.urls import url


urlpatterns = [
    url(r'^edit/$', 'students.views.edit_student', name='edit_student'),
    url(r'^settings/$', 'students.views.student_settings', name='student_settings'),
    url(r'^(?P<username>\w+)/$', 'students.views.student_profile', name='student_profile'),
    url(r'^open-paper-modal/$', 'students.views.openmodal', name='openmodal'),
    url(r'^load-more-paper/$', 'students.views.load_more_paper', name='load_more_paper'),
    url(r'^load-more-quotes/$', 'students.views.load_more_quotes', name='load_more_quotes'),
    url(r'^load-more-activities/$', 'students.views.load_more_activities', name='load_more_activities'),

]
