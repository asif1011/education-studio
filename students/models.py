from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django_countries.fields import CountryField
from editors import FINANCIAL_CHOICES
from django.conf import settings

from django.core.files.storage import FileSystemStorage

media_storage = FileSystemStorage(location=settings.BASE_DIR, base_url="/media/")


class Students(models.Model):
    user = models.ForeignKey(User)
    avatar = models.ImageField(upload_to="profile/students/", null=True, blank=True)
    nationality = models.CharField(max_length=255, blank=True, null=True)
    country = CountryField(blank_label="(select country)", null=True, blank=True)

    def __str__(self):
        return u'%s %s' %(self.user.first_name, self.user.last_name)


class StudentFinancialActivity(models.Model):
    student = models.ForeignKey(Students, related_name="student")
    paper = models.ForeignKey('editors.PaperRequest', related_name="student_paper_request", null=True, blank=True)
    description = models.CharField(max_length=255)
    amount = models.IntegerField(default=0)
    type = models.CharField(max_length=10, choices=FINANCIAL_CHOICES)
    charge_id = models.CharField(max_length=255, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'%s' % self.student

    def get_dollar_amount(self):
        return '{:.2f}'.format(self.amount)