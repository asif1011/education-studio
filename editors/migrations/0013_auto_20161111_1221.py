# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-11-11 12:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('editors', '0012_paperrequest_password'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paperrequest',
            name='status',
            field=models.CharField(choices=[('Default', 'Default'), ('Pending', 'Pending'), ('Progress', 'Progress'), ('Ready To Review', 'Ready To Review'), ('Approved', 'Approved'), ('Completed', 'Completed')], default='Default', max_length=60),
        ),
    ]
