import datetime

from django.core.management.base import BaseCommand, CommandError
from editors.models import PaperRequest, SiteConfig
from eseditor.utils import SendEmail


class Command(BaseCommand):
    help = 'Auto Approved the Paper at Deadline'

    def handle(self, *args, **options):
        all_paper_request = PaperRequest.objects.filter(status="Ready To Review")
        site_config =  SiteConfig.objects.get()
        for paper in all_paper_request:
            if (datetime.datetime.now() - paper.modified.replace(tzinfo=None)).days >= site_config.auto_approving_days:
                students_emails = [paper.student.user.email]
                editor = paper.editor
                student = paper.student
                msg = SendEmail()
                msg.send(recipient=students_emails, template_path='email_messages/auto_approved.html',
                    context={
                        "editor": editor,
                        "student": student,
                        "paper": paper
                    },
                    subject='Auto Approved Paper!'
                )
                paper.status = "Approved"
                paper.save()

    
