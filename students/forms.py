from __future__ import unicode_literals
from datetime import datetime

from django import forms
from django.conf import settings
from django.forms import FileInput
from django_countries import countries
from django.contrib.auth.models import User
from editors.utils import get_year_drop_down
from students.models import Students


class EditStudentForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control input-sm', 'type': 'email'}))
    default_avatar = forms.CharField(widget=forms.HiddenInput(), required=False)


    class Meta:
        model=Students
        exclude = ['user']
        widgets = {
            'avatar': FileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(EditStudentForm, self).__init__(*args, **kwargs)
        self.fields['country'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['nationality'].widget.attrs.update({'class': 'form-control input-sm'})
       