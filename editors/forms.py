from __future__ import unicode_literals
from datetime import datetime
import re

from django import forms
from django.conf import settings
from django.forms import FileInput
from django_countries import countries
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from editors.models import MONTH_CHOICES, RESEARCH_METHOD_CHOICES, REFERENCING_CHOICES, DEGREE_LEVEL_CHOICES
from editors.utils import get_year_drop_down
from editors.models import Editors, PaperRequest, EXP_YEAR, EXP_MONTH, Discipline, Answer, TestQuestion


class EditorFormStep1(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    phone_no = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm', 'maxlength': '15'}))
    research_methods = forms.CharField(widget=forms.Select(choices=RESEARCH_METHOD_CHOICES, attrs={'class': 'form-control input-sm'}))
    referencing_style = forms.CharField(widget=forms.Select(choices=REFERENCING_CHOICES, attrs={'class': 'form-control input-sm'}))
    optional_research_methods = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=False)
    optional_referencing_style = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=False)
    discipline = forms.MultipleChoiceField(widget=forms.Select(attrs={'class': 'form-control input-xs', 'multiple': 'multiple'}))
    research_interest = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=False)
    avatar = forms.ImageField(required=False)
    identity_document = forms.FileField(required=False)
    address_document = forms.FileField(required=False)
    qualification_document = forms.FileField(required=False)
    street1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=True)
    street2 =forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=False)
    city  = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=True)
    state = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=True)
    postal_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=False)
    country = forms.CharField(widget=forms.Select(choices=countries, attrs={'class': 'form-control input-sm'}), required=True)
    nationality = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=False)
    bio = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control input-sm', 'cols': '0', 'rows': '0'}), required=False)
    default_avatar = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(EditorFormStep1, self).__init__(*args, **kwargs)
        self.fields['discipline'] = forms.MultipleChoiceField(
            choices=[(discipline.id, str(discipline.name)) for discipline in Discipline.objects.all()]
        )
        self.fields['discipline'].required = True
        

    def clean_email(self):
        try:
            existing_user = User.objects.get(email__iexact=self.cleaned_data['email'])
            if existing_user:
                self._errors["email"] = self.error_class(["An account already exists under this email address. Please use the forgot password function to log in."])
        except User.MultipleObjectsReturned:
            self._errors["email"] = self.error_class(["An account already exists under this email address. Please use the forgot password function to log in."])
        except:
            pass

        return self.cleaned_data['email']
        

    def clean_optional_research_methods(self):
        research_methods = self.cleaned_data.get('research_methods')
        optional_research_methods = self.cleaned_data.get("optional_research_methods", "")

        if research_methods == "Others":
            if not optional_research_methods:
                raise forms.ValidationError('This field is required.')

        return optional_research_methods

    def clean_optional_referencing_style(self):
        referencing_style = self.cleaned_data.get('referencing_style')
        optional_referencing_style = self.cleaned_data.get("optional_referencing_style", "")

        if referencing_style == "Others":
            if not optional_referencing_style:
                raise forms.ValidationError('This field is required.')

        return optional_referencing_style


class TestForm(forms.Form):
    question_id = forms.CharField(widget=forms.HiddenInput())
    answer = forms.ModelChoiceField(queryset=Answer.objects.all(), widget=forms.RadioSelect())

    def __init__(self, *args, **kwargs):
        question = kwargs.pop('question', None)
        super(TestForm, self).__init__(*args, **kwargs)
        self.fields['answer'].queryset = Answer.objects.filter(question=question)
        self.fields['answer'].empty_label = None
        

class QualificationForm(forms.Form):
    degree = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    university = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    from_month = forms.CharField(widget=forms.Select(choices=MONTH_CHOICES, attrs={'class': 'form-control input-sm'}))
    from_year = forms.CharField(widget=forms.Select(choices=get_year_drop_down(), attrs={'class': 'form-control input-sm'}))
    to_month = forms.CharField(widget=forms.Select(choices=MONTH_CHOICES, attrs={'class': 'form-control input-sm'}))
    to_year = forms.CharField(widget=forms.Select(choices=get_year_drop_down(), attrs={'class': 'form-control input-sm'}))
    degree_level = forms.CharField(widget=forms.Select(choices=DEGREE_LEVEL_CHOICES, attrs={'class': 'form-control input-sm'}))
    qualification_document = forms.FileField(required=False)


class ExperienceForm(forms.Form):
    company_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    location = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'cols': '0', 'rows': '0' }))
    is_current = forms.BooleanField(required=False)
    from_month = forms.CharField(widget=forms.Select(choices=MONTH_CHOICES, attrs={'class': 'form-control input-sm'}))
    from_year = forms.CharField(widget=forms.Select(choices=get_year_drop_down(), attrs={'class': 'form-control input-sm'}))
    to_month = forms.CharField(widget=forms.Select(choices=MONTH_CHOICES, attrs={'class': 'form-control input-sm'}))
    to_year = forms.CharField(widget=forms.Select(choices=get_year_drop_down(), attrs={'class': 'form-control input-sm'}))


class QuoteForm(forms.Form):
    editor_id = forms.CharField(widget=forms.HiddenInput())
    discipline = forms.CharField(widget=forms.Select(attrs={'class': 'form-control multiselect input-sm myinputclass', 'required': 'required'}))
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Title of the Paper', 'maxlength': '100'}))
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control input-sm myinputclass', 'rows': 5, 'placeholder': 'Description of the Paper', 'maxlength': '1000'}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'First Name', 'maxlength': '30'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Last Name', 'maxlength': '30'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Email', 'maxlength': '75'}))
    words_count = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Words count of the paper', 'maxlength': '10'}))
    document = forms.FileField(required=False)


class ShortQuoteForm(forms.Form):
    editor_id = forms.CharField(widget=forms.HiddenInput())
    discipline = forms.CharField(widget=forms.Select(attrs={'class': 'form-control multiselect input-sm myinputclass', 'required': 'required'}))
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Title of the Paper', 'maxlength': '100'}))
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control input-sm myinputclass', 'rows': 5, 'placeholder': 'Description of the Paper', 'maxlength': '1000'}))
    words_count = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Words count of the paper', 'maxlength': '10'}))
    document = forms.FileField(required=False)


class PaperForm(forms.Form):
    editor_id = forms.CharField(widget=forms.HiddenInput())
    discipline = forms.CharField(widget=forms.Select(attrs={'class': 'form-control multiselect input-sm myinputclass', 'required': 'required'}))
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder' : 'Title of the Paper', 'maxlength': '100'}))
    price = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Price (%s)' % settings.PLATFORM_CURRENCY_DISPLAY}))
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control input-sm myinputclass', 'rows': 5, 'placeholder': 'Description of the Paper', 'maxlength': '1000'}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'First Name', 'maxlength': '30'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Last Name', 'maxlength': '30'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Email', 'maxlength': '75'}))
    deadline = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Deadline', 'autocomplete': 'off', 'readonly':'true'}))
    words_count = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Words count of the paper', 'maxlength': '10'}))
    paper = forms.FileField()

    def clean_price(self):
        price = self.cleaned_data.get('price')

        try:
            price = float(price)
        except:
            raise forms.ValidationError('Enter a number.')

        return price


class ShortPaperForm(forms.Form):
    editor_id = forms.CharField(widget=forms.HiddenInput())
    discipline = forms.CharField(widget=forms.Select(attrs={'class': 'form-control multiselect input-sm myinputclass', 'required': 'required'}))
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Title of the Paper', 'maxlength': '100'}))
    price = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Price (%s)' % settings.PLATFORM_CURRENCY_DISPLAY}))
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control input-sm myinputclass', 'rows': 5, 'placeholder': 'Description of the Paper', 'maxlength': '1000'}))
    deadline = forms.DateField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Deadline', 'autocomplete': 'off', 'readonly':'true'}))
    words_count = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm myinputclass', 'placeholder': 'Words count of the paper', 'maxlength': '10'}))
    paper = forms.FileField()

    def clean_price(self):
        price = self.cleaned_data.get('price')

        try:
            price = float(price)
        except:
            raise forms.ValidationError('Enter a number.')

        return price


class CompletePaperForm(forms.Form):
    paper_id = forms.CharField(widget=forms.HiddenInput())
    edited_paper = forms.FileField()


class PaperPriceForm(forms.Form):
    paper_id = forms.CharField(widget=forms.HiddenInput())
    price = forms.FloatField(widget=forms.TextInput(attrs={'class': 'form-control reset'}))


class AddToCartForm(forms.Form):
    paper_id = forms.CharField(widget=forms.HiddenInput())
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control reset', 'autocomplete': 'off'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_password(self):
        paper_id = self.cleaned_data.get('paper_id')
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get("password", "")

        paper = PaperRequest.objects.get(id=paper_id)

        if email != paper.email or password != str(paper.get_password_value()):
            raise forms.ValidationError("Email or Password didn't match!")

        return password


class EditEditorForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control input-sm', 'type': 'email', 'readonly': 'readonly'}))
    research_interest = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-sm'}), required=False)
    discipline = forms.MultipleChoiceField(widget=forms.Select(attrs={'class': 'form-control input-sm'}))
    default_avatar = forms.CharField(widget=forms.HiddenInput(), required=False)


    class Meta:
        model=Editors
        exclude = ['user', 'rating_star', 'is_verified']
        widgets = {
            'avatar': FileInput(),
            'identity_document': FileInput(attrs={'style': 'padding-top: 15px'}),
            'qualification_document': FileInput(attrs={'style': 'padding-top: 15px'}),
            'address_document': FileInput(attrs={'style': 'padding-top: 15px'}),
        }

    def __init__(self, *args, **kwargs):
        super(EditEditorForm, self).__init__(*args, **kwargs)
        self.fields['phone_no'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['research_methods'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['optional_research_methods'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['referencing_style'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['optional_referencing_style'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['street1'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['street2'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['city'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['state'].widget.attrs.update({'class': 'form-control input-sm', 'cols': '', 'rows': ''})
        self.fields['postal_code'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['country'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['nationality'].widget.attrs.update({'class': 'form-control input-sm'})
        self.fields['bio'].widget.attrs.update({'class': 'form-control', 'rows': '0', 'cols': '0', 'maxlength': '500'})
        self.fields['discipline'] = forms.MultipleChoiceField(
            choices=[(discipline.id, str(discipline.name)) for discipline in Discipline.objects.all()]
        )
        self.fields['phone_no'].required = True
        self.fields['research_methods'].required = True
        self.fields['optional_research_methods'].required = False
        self.fields['referencing_style'].required = True
        self.fields['optional_referencing_style'].required = False
        self.fields['street1'].required = True
        self.fields['city'].required = True
        self.fields['state'].required = True

    def clean_optional_research_methods(self):
        research_methods = self.cleaned_data.get('research_methods')
        optional_research_methods = self.cleaned_data.get("optional_research_methods", "")

        if research_methods == "Others":
            if not optional_research_methods:
                raise forms.ValidationError('This field is required.')

        return optional_research_methods

    def clean_optional_referencing_style(self):
        referencing_style = self.cleaned_data.get('referencing_style')
        optional_referencing_style = self.cleaned_data.get("optional_referencing_style", "")

        if referencing_style == "Others":
            if not optional_referencing_style:
                raise forms.ValidationError('This field is required.')

        return optional_referencing_style


class GetPaidForm(forms.Form):
    amount = forms.FloatField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(GetPaidForm, self).__init__(*args, **kwargs)

    def clean_amount(self):
        super(GetPaidForm, self).clean()
        amount = self.cleaned_data.get('amount')

        editor = Editors.objects.get(user=self.user)
        editor_balance = float(editor.get_total_balance())

        if amount > editor_balance:
            raise forms.ValidationError("Please enter the amount same or less than the balance amount.")

        return self.cleaned_data['amount']
