import urllib, urllib2
import json
from django.http import HttpResponseRedirect
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from editors.models import StripeProfile


@login_required
def stripe_connect(request):
    stripe_auth_url = '%s?response_type=code&client_id=%s&redirect_uri=%s/editors/stripe/authenticated/&scope=read_write' % (
        settings.STRIPE_AUTHORIZE_URL,
        settings.STRIPE_CLIENT_ID,
        settings.SITE_URL
    )

    return HttpResponseRedirect(stripe_auth_url)


def get_access_token(code):
    auth_url = settings.STRIPE_TOKEN_URL
    body = urllib.urlencode({
        'code': code,
        'client_secret': settings.STRIPE_SECRET_KEY,
        'grant_type': 'authorization_code'
    })

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }

    req = urllib2.Request(auth_url, body, headers)
    resp = urllib2.urlopen(req)

    data = json.loads(resp.read())

    return data


def stripe_authenticated(request):
    if 'error' in request.GET:
        messages.error(request, request.GET['error_description'])
        return HttpResponseRedirect(reverse("settings")+"?financial_activities_link=true")

    if 'code' not in request.GET:
        messages.error(request, 'No code returned from Stripe')
        return HttpResponseRedirect(reverse("settings")+"?financial_activities_link=true")

    code = request.GET['code']
    
    try:
        data = get_access_token(code)
        access_token = data['access_token']
        stripe_id = data['stripe_user_id']
        publishable_key = data['stripe_publishable_key']
    except Exception as e:
        messages.error(request, str(e))
        return HttpResponseRedirect(reverse("settings")+"?financial_activities_link=true")

    try:
        profile = StripeProfile.objects.get(stripe_id=stripe_id)
        if access_token != profile.access_token:
            profile.access_token = access_token
            profile.save()

    except StripeProfile.DoesNotExist:
        profile = StripeProfile.objects.create(user=request.user, access_token=access_token, stripe_id=stripe_id, publishable_key=publishable_key)

    except StripeProfile.MultipleObjectsReturned:
        profile = StripeProfile.objects.filter(stripe_id=stripe_id)[0]
        if access_token != profile.access_token:
            profile.access_token = access_token
            profile.save()

    return HttpResponseRedirect('/editors/settings/')
