from django.contrib import admin

from .models import Message

class MessageAdmin(admin.ModelAdmin):
    list_display = ['sender', 'recipient', 'sender_read', 'recipient_read', 'subject', 'message', 'created' ]
    search_fields = ['created', 'recipient']


admin.site.register(Message, MessageAdmin)
