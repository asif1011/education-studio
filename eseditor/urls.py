from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.auth import views
from django.views.static import serve
from django.conf import settings


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    # Home Url
    url(r'^$', 'editors.views.editing_services', name='editing_services'),
    url(r'^about-us/$', 'accounts.views.about_us', name='about_us'),
    url(r'^contact-us/$', 'accounts.views.contact_us', name='contact_us'),
    url(r'^newsletter/$', 'accounts.views.newsletter', name='newsletter'),

    url(r'^editors/$', 'editors.views.editor_home', name='home'),
    url(r'^load_more_editors/$', 'editors.views.load_more_editors', name='load_more_editors'),
    url(r'^load-more-edited-paper/$', 'editors.views.load_more_edited_paper', name='load_more_edited_paper'),
    url(r'^load-more-paper-request/$', 'editors.views.load_more_paper_request', name='load_more_paper_request'),
    url(r'^load-more-quotes-request/$', 'editors.views.load_more_quotes_request', name='load_more_quotes_request'),
    url(r'^load-more-fiancial-activites/$', 'editors.views.load_more_fiancial_activities', name='load_more_fiancial_activities'),
    url(r'^job-page/paper/(?P<paper_id>[0-9]+)/$', 'editors.views.paper_detail', name='paper_detail'),

    # App Url
    url(r'editors/', include('editors.urls')),
    url(r'student/', include('students.urls')),
    url(r'accounts/', include('accounts.urls')),
    url(r'messages/', include('message.urls')),

    # Logout Url
    url(r'^logout/$', views.logout, {'next_page': settings.LOGIN_URL}, name="logout"),

    # Reset Password Url
    url(r'^user/password/reset/done/$',
        'django.contrib.auth.views.password_reset_done',
         name='password_reset_done'),

    # Rating URLs
    url(r'^ratings/', include('star_ratings.urls', namespace='ratings', app_name='ratings')),

    # checkout Url
    url(r'^checkout/$', 'editors.views.checkout', name='checkout'),

    # Media Url
    url(r'^media/(?P<path>.*)', serve, {"document_root": settings.MEDIA_ROOT})
]
