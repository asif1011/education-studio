from __future__ import unicode_literals

from django import forms
from django.conf import settings
from django.contrib.auth.models import User

from message.models import Message

class MessageForm(forms.ModelForm):
    
    fields = ['sender', 'recipient', 'parent_msg', 'subject', 'message']

    class Meta:
        model=Message
        exclude = ['sender_read', 'recipient_read', 'created']
        widgets = {
            'sender': forms.HiddenInput(),
            'recipient': forms.HiddenInput(),
            'parent_msg': forms.HiddenInput(),
            'subject': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)
        self.fields['message'].widget.attrs.update({'class': 'form-control', 'cols': '0', 'rows': '4'})
        self.fields["message"].required = True