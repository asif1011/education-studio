from django.contrib import admin
from django import forms
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from accounts.models import PasswordResetAuth, Newsletter


admin.site.unregister(User)


class CustomUserChangeForm(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(CustomUserChangeForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True

    def clean_email(self):
        data = self.cleaned_data['email']
        if User.objects.filter(email__iexact=data).exclude(email=self.instance.email).exists():
            raise forms.ValidationError("This email already exists!")
        return data


class CustomUserAddForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(CustomUserAddForm, self).__init__(*args, **kwargs)


class CustomUserAdmin(UserAdmin):
    list_display = ['username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active']
    ordering = ['-date_joined']
    add_form = CustomUserAddForm
    form = CustomUserChangeForm


class PasswordResetAuthAdmin(admin.ModelAdmin):
    list_display = ['email', 'token', 'is_expired']
    search_fields = ['email', 'token']


class NewsletterAdmin(admin.ModelAdmin):
    list_display = ['email']
    search_fields = ['email']


admin.site.register(User, CustomUserAdmin)
admin.site.register(PasswordResetAuth, PasswordResetAuthAdmin)
admin.site.register(Newsletter, NewsletterAdmin)
