from __future__ import unicode_literals

from decimal import Decimal
from django import template
from django.contrib.contenttypes.models import ContentType

from star_ratings.models import Rating, UserRating
from editors.utils import calculate_rating_stars
from editors.models import Editors, PaperRequest, RatingFeedback

register = template.Library()


@register.inclusion_tag('editors/stars.html', takes_context=True)
def editor_stars(context, editor_id):
    editor = Editors.objects.get(id=editor_id)

    score = editor.rating_star
    has_rating = False

    if score:
        has_rating = True

    score = float(score)
    star = int(score)
    semi_star = not score.is_integer()
    star_empty = 4 - int(score) if semi_star else 5 - int(score)

    return {
        'star': range(star),
        'semi_star': semi_star,
        'star_empty': range(star_empty),
        'has_rating': has_rating
    }


@register.inclusion_tag('editors/stars.html', takes_context=True)
def paper_stars(context, paper_id, detail_page=None):
    content_type = ContentType.objects.get_for_model(PaperRequest)
    ratings = Rating.objects.filter(content_type=content_type, object_id__in=[paper_id])

    if not detail_page:
        feedback = RatingFeedback.objects.get(rating=ratings[0])
    else:
        feedback = None

    score, star, semi_star, star_empty, has_rating = calculate_rating_stars(ratings=ratings)

    return {
        'star': range(star),
        'semi_star': semi_star,
        'star_empty': range(star_empty),
        'has_rating': has_rating,
        'feedback': feedback
    }

@register.filter
def get_serial_no(count, start):
    return int(start)+int(count)
