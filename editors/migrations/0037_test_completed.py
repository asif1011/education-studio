# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-02-02 19:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('editors', '0036_auto_20170203_0043'),
    ]

    operations = [
        migrations.AddField(
            model_name='test',
            name='completed',
            field=models.BooleanField(default=False),
        ),
    ]
