# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-11-03 11:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('editors', '0005_auto_20161102_1318'),
    ]

    operations = [
        migrations.CreateModel(
            name='Quotation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, null=True)),
                ('name', models.CharField(max_length=255)),
                ('email', models.CharField(max_length=255)),
                ('words_count', models.CharField(max_length=255)),
                ('editor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='editors.Editors')),
            ],
        ),
        migrations.AlterField(
            model_name='experience',
            name='from_month',
            field=models.CharField(blank=True, choices=[('january', 'January'), ('february', 'February'), ('march', 'March'), ('april', 'April'), ('may', 'May'), ('june', 'June'), ('july', 'July'), ('august', 'August'), ('september', 'September'), ('october', 'October'), ('november', 'November'), ('december', 'December')], max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='experience',
            name='to_month',
            field=models.CharField(blank=True, choices=[('january', 'January'), ('february', 'February'), ('march', 'March'), ('april', 'April'), ('may', 'May'), ('june', 'June'), ('july', 'July'), ('august', 'August'), ('september', 'September'), ('october', 'October'), ('november', 'November'), ('december', 'December')], max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='qualification',
            name='from_month',
            field=models.CharField(blank=True, choices=[('january', 'January'), ('february', 'February'), ('march', 'March'), ('april', 'April'), ('may', 'May'), ('june', 'June'), ('july', 'July'), ('august', 'August'), ('september', 'September'), ('october', 'October'), ('november', 'November'), ('december', 'December')], max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='qualification',
            name='to_month',
            field=models.CharField(blank=True, choices=[('january', 'January'), ('february', 'February'), ('march', 'March'), ('april', 'April'), ('may', 'May'), ('june', 'June'), ('july', 'July'), ('august', 'August'), ('september', 'September'), ('october', 'October'), ('november', 'November'), ('december', 'December')], max_length=20, null=True),
        ),
    ]
