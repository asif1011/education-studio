# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-12-06 11:49
from __future__ import unicode_literals

import autoslug.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('editors', '0025_remove_financialactivity_student'),
    ]

    operations = [
        migrations.CreateModel(
            name='Discipline',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='name', unique=True)),
                ('name', models.CharField(max_length=40)),
            ],
        ),
        migrations.AddField(
            model_name='editors',
            name='discipline',
            field=models.ManyToManyField(to='editors.Discipline'),
        ),
    ]
