from __future__ import unicode_literals

import datetime
import math
from decimal import Decimal

from django.contrib.auth.models import User
from django.conf import settings

from star_ratings.models import Rating, UserRating


def get_year_drop_down():
    YEAR_DROP_DOWN = []

    for year in range(1940, (datetime.datetime.now().year) + 1):
        YEAR_DROP_DOWN.append((str(year), str(year)))

    return YEAR_DROP_DOWN[::-1]


def create_username(name):
    """create the unique username of every new user by using its name"""
    name = ''.join(e for e in name if e.isalnum())
    name = name[:29]
    base_name = name
    ctr = 1

    while True:
        try:
            user = User.objects.get(username=name)
            name = base_name + (str(ctr))
            ctr += 1
        except User.DoesNotExist:
            break

    return name


def get_service_fee(price):
    return math.ceil(price*settings.PLATFORM_FEE/100.0)


def get_charge_price(price):
    platform_fee = get_service_fee(price)
    return math.ceil(price+platform_fee)


def get_transfer_price(price):
    platform_fee = get_service_fee(price)
    return math.ceil(price-platform_fee)


def calculate_rating_stars(ratings=[]):
    score = 0
    has_rating = False
        
    for rating in ratings:
       score += rating.average

    if ratings:
        score = score/len(ratings)
        has_rating = True

    score = float(score)
    star = int(score)
    semi_star = not score.is_integer()
    star_empty = 4 - int(score) if semi_star else 5 - int(score)

    return score, star, semi_star, star_empty, has_rating
