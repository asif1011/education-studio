from django.conf import settings
from django.shortcuts import render
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.core.urlresolvers import reverse
import datetime

from message.models import Message
from editors.models import Editors, PaperRequest
from students.models import Students
from eseditor.utils import SendEmail

from message.forms import MessageForm


@login_required(login_url=settings.LOGIN_URL)
def message(request):
    user = request.user

    all_msg_objs = Message.objects.filter(Q(recipient=user) | Q(sender=user)).filter(parent_msg__isnull=True ).order_by('-created')
    
    all_msg_objs = list(all_msg_objs)

    all_msg_objs.sort(key=lambda x: x.get_last_child().created, reverse=True)

    return render_to_response("message/messages.html", {
        "current" : "messages",
        'msg_objs': all_msg_objs
    }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def message_details(request, msg_id):
    
    msg_obj = Message.objects.get(id=msg_id)

    if msg_obj.recipient == request.user:
        msg_obj.recipient_read = True
    else:
        msg_obj.sender_read = True
    msg_obj.save()

    child_msg = msg_obj.parent.all()
    child_msg.filter(recipient=request.user).update(recipient_read=True)
    child_msg.filter(sender=request.user).update(sender_read=True)

    if msg_obj.sender == request.user:
        recipient = msg_obj.recipient
    else:
        recipient = msg_obj.sender

    if request.method == "POST":
        form = MessageForm(request.POST)

        if form.is_valid():
            msg_obj = Message.objects.get(id=msg_id)

            message = form.cleaned_data['message']

            new_msg_obj = form.save()

            msg = SendEmail(request)
            msg.send(recipient=[new_msg_obj.recipient.email], template_path='email_messages/msg_notification.html',
                context={
                    'msg_link': settings.SITE_URL+reverse("message_details", kwargs={"msg_id": msg_obj.id}),
                    'recipient_name': new_msg_obj.recipient.first_name,
                    'new_msg_obj': new_msg_obj
                },
                subject='[Education Studio] New message received!'
            )
        return redirect(reverse('message_details', kwargs={'msg_id':  msg_obj.id}))

    else:
        if request.user == msg_obj.recipient or request.user == msg_obj.sender:
            form = MessageForm(initial={
                "sender": request.user.id,
                "recipient": recipient.id,
                "subject": msg_obj.subject,
                "parent_msg": msg_obj.parent_msg.id if msg_obj.parent_msg else msg_obj.id
            })

            return render_to_response("message/details.html", {
                'current': "conversations",
                'form': form,
                'msg_obj': msg_obj,
                'child_msg': child_msg
            }, context_instance=RequestContext(request))
        else:
            return redirect(reverse('message'))    
