from __future__ import unicode_literals

import os
import stripe
from datetime import datetime

from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django_countries.fields import CountryField
from django.core.files.storage import FileSystemStorage
from autoslug import AutoSlugField
from star_ratings.models import Rating

from editors.utils import get_year_drop_down, get_service_fee
from editors.encryption import decrypt_aes
from students.models import Students
from editors import FINANCIAL_CHOICES
from solo.models import SingletonModel

media_storage = FileSystemStorage(location=settings.BASE_DIR, base_url="/media/")

stripe.api_key = settings.STRIPE_SECRET_KEY

YEAR_DROP_DOWN = get_year_drop_down()

MONTH_CHOICES = (
    ('january', 'January'),
    ('february', 'February'),
    ('march', 'March'),
    ('april', 'April'),
    ('may', 'May'),
    ('june', 'June'),
    ('july', 'July'),
    ('august', 'August'),
    ('september', 'September'),
    ('october', 'October'),
    ('november', 'November'),
    ('december', 'December'),
)

RESEARCH_METHOD_CHOICES = (
    ('Quantitative', 'Quantitative'),
    ('Qualitative', 'Qualitative'),
    ('Both', 'Both'),
    ('Others', 'Others'),
)

DEGREE_LEVEL_CHOICES = (
    ('Bachelor', 'Bachelor'),
    ('Masters', 'Masters'),
    ('PhD', 'PhD'),
)

PAPER_STATUS_CHOICES = (
    ('Draft', 'Draft'),
    ('Rejected', 'Rejected'),
    ('Pending', 'Pending'),
    ('Progress', 'Progress'),
    ('Ready To Review', 'Ready To Review'),
    ('Approved', 'Approved'),
    ('Completed', 'Completed'),
)

REFERENCING_CHOICES = (
    ('Harvard', 'Harvard'),
    ('Oxford', 'Oxford'),
    ('Chicago', 'Chicago'),
    ('APA', 'APA'),
    ('MLA', 'MLA'),
    ('Others', 'Others'),
)

DIFFICULTY_LEVEL = (
    ('EASY', 'EASY'),
    ('MEDIUM', 'MEDIUM'),
    ('DIFFICULT', 'DIFFICULT'),
)

EXP_YEAR = [(x, x) for x in range(datetime.today().date().year, datetime.today().date().year + 15)]

EXP_MONTH = [
    ('1', 'January'), ('2', 'February'), ('3', 'March'), ('4', 'April'), ('5', 'May'), ('6', 'June'),
    ('7', 'July'), ('8', 'August'), ('9', 'September'), ('10', 'October'), ('11', 'November'), ('12', 'December'),
]


class ResearchInterest(models.Model):
    slug = AutoSlugField(populate_from='interest', unique=True)
    interest = models.CharField(max_length=40)

    def __str__(self):
        return u'%s' %(self.interest)


class Discipline(models.Model):
    slug = AutoSlugField(populate_from='name', unique=True)
    name = models.CharField(max_length=40)

    def __str__(self):
        return u'%s' %(self.name)


class Editors(models.Model):
    user = models.ForeignKey(User)
    avatar = models.ImageField(upload_to="profile/editors/", null=True, blank=True)
    phone_no = models.CharField(max_length=15, blank=True, null=True)
    street1 = models.CharField(max_length=60, blank=True, null=True)
    street2 = models.CharField(max_length=60, blank=True, null=True)
    city  = models.CharField(max_length=30, blank=True, null=True)
    state = models.CharField(max_length=30, blank=True, null=True)
    postal_code = models.CharField(max_length=12, blank=True, null=True)
    nationality = models.CharField(max_length=255, blank=True, null=True)
    bio = models.TextField(null=True, blank=True)
    research_methods = models.CharField(max_length=60, null=True, blank=True, choices=RESEARCH_METHOD_CHOICES)
    referencing_style = models.CharField(max_length=60, null=True, blank=True, choices=REFERENCING_CHOICES)
    optional_research_methods = models.CharField(max_length=60, null=True, blank=True)
    optional_referencing_style = models.CharField(max_length=60, null=True, blank=True)
    discipline = models.ManyToManyField(Discipline)
    research_interest = models.ManyToManyField(ResearchInterest)
    identity_document = models.FileField(upload_to="documents/identity/", null=True, blank=True)
    address_document = models.FileField(upload_to="documents/address/", null=True, blank=True)
    qualification_document = models.FileField(upload_to="documents/qualification/", null=True, blank=True)
    country = CountryField(blank_label="(select country)", null=True, blank=True)
    rating_star = models.FloatField(default=0.0)
    is_verified = models.BooleanField(default=False)

    def __str__(self):
        return u'%s %s' %(self.user.first_name, self.user.last_name)

    def get_identity_document(self):
        return os.path.basename(self.identity_document.name)

    def get_address_document(self):
        return os.path.basename(self.address_document.name)

    def get_qualification_document(self):
        return os.path.basename(self.qualification_document.name)

    def get_total_balance(self):
        total = 0
        for activity in self.editor.all():
            if activity.type == "Credit":
                total += activity.amount
            else:
                total -= activity.amount
        return total


class ProfileViewer(models.Model):
    editor = models.ForeignKey(Editors)
    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    difficulty_level = models.CharField(max_length=60, null=True, blank=True, choices=DIFFICULTY_LEVEL)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.question_text


class Answer(models.Model):
    question = models.ForeignKey(Question)
    answer_text = models.CharField(max_length=200)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return self.answer_text


class Test(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    editor = models.ForeignKey(Editors)
    finished = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    title = models.CharField(max_length=255, null=True, blank=True)
    score = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return '%s for %s' % (self.title, self.editor.user.get_full_name())


class TestQuestion(models.Model):
    question = models.ForeignKey(Question, related_name="question")
    answer = models.ForeignKey(Answer, related_name="answer", null=True, blank=True)
    test = models.ForeignKey(Test, related_name="test")
    is_attempted = models.BooleanField(default=False)

    def __str__(self):
        return '%s - %s' % (self.question.question_text, self.test.title)


class Qualification(models.Model):
    editor = models.ForeignKey(Editors)
    degree = models.CharField(max_length=30, blank=True, null=True)
    university = models.CharField(max_length=30, blank=True, null=True)
    from_month = models.CharField(max_length=20, null=True, blank=True, choices=MONTH_CHOICES)
    from_year = models.CharField(max_length=4, null=True, blank=True, choices=YEAR_DROP_DOWN)
    to_month = models.CharField(max_length=20, null=True, blank=True, choices=MONTH_CHOICES)
    to_year = models.CharField(max_length=4, null=True, blank=True, choices=YEAR_DROP_DOWN)
    degree_level = models.CharField(max_length=20, null=True, blank=True, choices=DEGREE_LEVEL_CHOICES)
    qualification_document = models.FileField(upload_to="documents/qualification/", null=True, blank=True)

    def __str__(self):
        return u'%s' %(self.editor)

    def get_from_month(self):
        return dict(MONTH_CHOICES).get(self.from_month)

    def get_to_month(self):
        return dict(MONTH_CHOICES).get(self.to_month)


class Experience(models.Model):
    editor = models.ForeignKey(Editors)
    company_name = models.CharField(max_length=32)
    title = models.CharField(max_length=70, null=True, blank=True)
    location = models.CharField(max_length= 50, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    is_current = models.BooleanField(default=False)
    from_month = models.CharField(max_length=20, null=True, blank=True, choices=MONTH_CHOICES)
    from_year = models.CharField(max_length=4, null=True, blank=True, choices=YEAR_DROP_DOWN)
    to_month = models.CharField(max_length=20, null=True, blank=True, choices=MONTH_CHOICES)
    to_year = models.CharField(max_length=4, null=True, blank=True, choices=YEAR_DROP_DOWN)

    def __str__(self):
        return u'%s' %(self.company_name)

    def get_from_month(self):
        return dict(MONTH_CHOICES).get(self.from_month)

    def get_to_month(self):
        return dict(MONTH_CHOICES).get(self.to_month)


class Quotation(models.Model):
    editor = models.ForeignKey(Editors)
    discipline = models.ForeignKey(Discipline, null=True, blank=True)
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    words_count = models.CharField(max_length=255)
    document = models.FileField(upload_to="papers/quotation-documents/", null=True, blank=True)
    student = models.ForeignKey(Students, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return u'%s requested quotes from %s' % (self.student, self.editor)


class PaperRequest(models.Model):
    editor = models.ForeignKey(Editors)
    discipline = models.ForeignKey(Discipline, null=True, blank=True)
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    words_count = models.CharField(max_length=255)
    price = models.FloatField(null=True, blank=True)
    price_paid = models.FloatField(null=True, blank=True)
    raw_paper = models.FileField(upload_to="papers/raw/")
    status = models.CharField(max_length=60, choices=PAPER_STATUS_CHOICES, default='Draft')
    charge_id = models.CharField(max_length=255, null=True, blank=True)
    student = models.ForeignKey(Students, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    deadline = models.DateField(null=True, blank=True)


    def __str__(self):
        return u'%s - %s' % (self.title, self.editor)

    def get_password_value(self):
        return decrypt_aes(self.password, settings.ENCRYPTION_SECRET_KEY)

    def get_dollar_price(self):
        return '{:.2f}'.format(self.price)

    def get_paper_complete_url(self):
        return reverse("paper_complete", kwargs={"paper_id": self.id})

    def has_rating(self):
        return Rating.objects.filter(content_type=ContentType.objects.get_for_model(PaperRequest), object_id=self.id).exists()


class PaperReview(models.Model):
    paper = models.ForeignKey(PaperRequest)
    edited_paper = models.FileField(upload_to="papers/edited/")
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, null=True, blank=True)


class RatingFeedback(models.Model):
    rating = models.OneToOneField(Rating)
    feedback = models.TextField(null=True, blank=True)

    def __str__(self):
        return u'feedback of %s' % self.rating


class Verification(models.Model):
    paper = models.ForeignKey(PaperRequest)
    token = models.CharField(max_length=11)
    is_expired = models.BooleanField(default=False)

    def __str__(self):
        return u'%s' % self.paper


class Membership(models.Model):
    editor = models.ForeignKey(Editors)
    customer_code = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'%s' % self.editor


class SiteConfig(SingletonModel):
    auto_approving_days = models.PositiveIntegerField(default=10)


class FinancialActivity(models.Model):
    editor = models.ForeignKey(Editors, related_name="editor")
    paper = models.ForeignKey(PaperRequest, related_name="paper_request", null=True, blank=True)
    description = models.CharField(max_length=255)
    amount = models.IntegerField(default=0)
    type = models.CharField(max_length=10, choices=FINANCIAL_CHOICES)
    transfer_id = models.CharField(max_length=255, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'%s' % self.editor

    def get_dollar_amount(self):
        return '{:.2f}'.format(self.amount)


class StripeProfile(models.Model):
    user = models.ForeignKey(User)
    stripe_id = models.CharField(max_length=200)
    access_token = models.CharField(max_length=200)
    publishable_key = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.get_full_name()


def create_activity(sender, instance, created, **kwargs):
    if instance.paper:
        FinancialActivity.objects.create(
            editor=instance.editor,
            description="Service Fee",
            type="Debit",
            amount=get_service_fee(instance.paper.price)
        )


class PaymentMethod(object):
    def __init__(self, user, card_code=None):
        self.user = user
        self.card_code = card_code

    def get_customer(self):
        try:
            subscription = Membership.objects.get(editor__user=self.user)
        except Membership.DoesNotExist:
            subscription = None

        if subscription:
            customer_code = subscription.customer_code
            customer = stripe.Customer.retrieve(customer_code)
        else:
            customer = None

        return customer

    def get(self):
        customer = self.get_customer()
        if customer:
            response = {'status': True, 'data': None, 'count': 1}
            if self.card_code:
                try:
                    card_detail = customer.sources.retrieve(self.card_code)
                    response['data'] = card_detail
                    response['default_card'] = customer['default_source']
                except:
                    response['msg'] = 'No Card exists with this Card Detail, Please contact support for assistance!'
            else:
                cards = customer.sources.all(limit=100, object='card')
                response['data'] = cards['data']
                response['default_card'] = customer['default_source']
                response['customer_id'] = customer['id']
        else:
            response = {'status': False}
        return response

    def add(self, token):
        customer = self.get_customer()
        response = {'status': True, 'data': None, 'count': 1}
        if customer:
            try:
                card_detail = customer.sources.create(card=token)
                response['data'] = card_detail
                response['default_card'] = customer['default_source']
            except stripe.CardError as err:
                response['status'] = False
                response['msg'] = str(err)
        else:
            try:
                desc = "Customer for %s and Email ID is %s" % (self.user.get_full_name(), self.user.email)
                customer = stripe.Customer.create(description=desc, source=token)
                editor = Editors.objects.get(user=self.user)
                membership = Membership.objects.create(
                    editor=editor,
                    customer_code=customer.id
                )
                response['data'] = customer.sources.data[0]
                response['default_card'] = customer['default_source']
            except stripe.CardError as err:
                response['status'] = False
                response['msg'] = str(err)
        return response

    def delete(self):
        response = {'status': False}
        customer = self.get_customer()
        if customer:
            try:
                s_response = customer.sources.retrieve(self.card_code).delete()
                if s_response['deleted']:
                    response = {'status': True}
            except:
                msg = 'No Card exists with this Card Detail, Please contact support for assistance!'
                response['msg'] = msg
        return response

    def make_primary(self):
        response = {'status': False}
        customer = self.get_customer()
        if customer:
            get_response = self.get()
            if get_response['status']:
                try:
                    customer.default_source = self.card_code
                    customer.save()
                    response = {'status': True}
                except:
                    response['msg'] = 'No Card exists with this Card Detail, Please contact support for assistance!'
        return response


models.signals.post_save.connect(create_activity, sender=FinancialActivity)
