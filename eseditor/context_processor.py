from editors.models import Editors
from students.models import Students


def user_info(request):
    if request.user.is_authenticated():
        try:
            student = Students.objects.get(user=request.user)
            context = {
                'role': 'student',
                'profile': student
            }
        except:
            try:
                editor = Editors.objects.get(user=request.user)
                context = {
                    'role': 'editor',
                    'profile': editor
                }
            except:
                context = {
                    'role': 'admin'
                }
    else:
        context = {}

    return context
