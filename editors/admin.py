from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import ResearchInterest, Editors, Qualification, Experience, Quotation, PaperRequest, Verification, Membership, FinancialActivity, StripeProfile, Discipline, RatingFeedback, Question, Answer, Test, TestQuestion, PaperReview, ProfileViewer, SiteConfig


class ProfileViewerAdmin(admin.ModelAdmin):
    list_display = ['user', 'editor', 'created', 'modified']
    search_fields = ['user']


class ResearchInterestAdmin(admin.ModelAdmin):
    list_display = ['interest']
    search_fields = ['interest']


class DisciplineAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']    


class AnswerInline(admin.StackedInline):
    model = Answer
    extra = 2


class QuestionAdmin(admin.ModelAdmin):
    list_display = ['question_text', 'difficulty_level', 'description']
    search_fields = ['question_text']
    inlines = [AnswerInline]


class AnswerAdmin(admin.ModelAdmin):
    list_display = ['answer_text', 'is_correct']
    search_fields = ['answer_text']   


class TestAdmin(admin.ModelAdmin):
    list_display = ['created', 'editor', 'finished', 'score', 'completed']
    search_fields = ['created', 'editor', 'finished', 'score']


class TestQuestionAdmin(admin.ModelAdmin):
    list_display = ['question', 'answer', 'test', 'is_attempted']
    search_fields = ['question', 'answer', 'test', 'is_attempted']    


class EditorsAdmin(admin.ModelAdmin):
    list_display = ['user', 'phone_no', 'city', 'state', 'country']
    filter_horizontal = ['research_interest', 'discipline']
    search_fields = ['user__first_name', 'user__last_name']


class QualificationAdmin(admin.ModelAdmin):
    list_display = ['editor', 'degree', 'university']
    search_fields = ['user__first_name', 'user__last_name', 'degree', 'university']


class ExperienceAdmin(admin.ModelAdmin):
    list_display = ['editor', 'company_name', 'title', 'location']
    search_fields = ['user__first_name', 'user__last_name', 'company_name', 'title', 'location']


class QuotationAdmin(admin.ModelAdmin):
    list_display = ['editor', 'title']
    search_fields = ['editor__user__first_name', 'editor__user__last_name', 'title']


class PaperRequestAdmin(admin.ModelAdmin):
    list_display = ['editor', 'title', 'status', 'created']
    list_filter = ('status', )
    search_fields = ['editor__user__first_name', 'editor__user__last_name', 'title']
    readonly_fields = ['created', 'modified']


class PaperReviewAdmin(admin.ModelAdmin):
    list_display = ['paper', 'edited_paper', 'created']
    search_fields = ['editor__user__first_name', 'editor__user__last_name', 'paper__title']    


class VerificationAdmin(admin.ModelAdmin):
    list_display = ['paper', 'token', 'is_expired']
    search_fields = ['paper', 'token']


class MembershipAdmin(admin.ModelAdmin):
    list_display = ['customer_code', 'editor']
    search_fields = ['customer_code']


class FinancialActivityAdmin(admin.ModelAdmin):
    list_display = ['editor', 'paper', 'description', 'amount', 'type', 'created']
    search_fields = ['editor__user__first_name', 'editor__user__last_name', 'paper__title', 'description', 'amount', 'type', 'created']


class StripeProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'stripe_id', 'created']
    search_fields = ['user__first_name', 'user__last_name']


class RatingFeedbackAdmin(admin.ModelAdmin):
    list_display = ['rating']


admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Test, TestAdmin)
admin.site.register(TestQuestion, TestQuestionAdmin)
admin.site.register(ResearchInterest, ResearchInterestAdmin)
admin.site.register(Discipline, DisciplineAdmin)
admin.site.register(Editors, EditorsAdmin)
admin.site.register(Qualification, QualificationAdmin)
admin.site.register(Experience, ExperienceAdmin)
admin.site.register(Quotation, QuotationAdmin)
admin.site.register(PaperRequest, PaperRequestAdmin)
admin.site.register(Verification, VerificationAdmin)
admin.site.register(Membership, MembershipAdmin)
admin.site.register(FinancialActivity, FinancialActivityAdmin)
admin.site.register(StripeProfile, StripeProfileAdmin)
admin.site.register(RatingFeedback, RatingFeedbackAdmin)
admin.site.register(PaperReview, PaperReviewAdmin)
admin.site.register(ProfileViewer, ProfileViewerAdmin)
admin.site.register(SiteConfig, SingletonModelAdmin)



