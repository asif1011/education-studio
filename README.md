# **Education Studio** #

Technologies used in the project are **Python2.7**, **Django1.9** and **Bootstrap**.

**Steps to Setup project locally:**

* Add your ssh key to the bitbucket for cloning the project

* Clone the project repository using SSH access and command will be
```
#!shell

git clone git@bitbucket.org:educationstudio/educationstudio.git
```

* Create the virtualenv by running
```
#!shell
virtualenv --no-site-packages venv

```

* Activate the virtual Environment
```
#!shell
source venv/bin/activate
```

* cd educationstudio

* Install all other dependencies by running
```
#!shell

pip install -r requirements.txt
```
* Run the migration command

```
#!shell

python manage.py migrate
```

* Run the server
```
#!shell

python manage.py runserver
```

* Check the http://localhost:8000 on your browser