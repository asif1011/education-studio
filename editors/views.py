import json, os
import random
from random import shuffle
import stripe
import requests

from datetime import date, datetime, time, timedelta

from string import ascii_uppercase
from PIL import Image

from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib.auth import authenticate, login, get_backends
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.crypto import get_random_string
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q, Count
from django.http import HttpResponse, Http404
from django_countries import countries
from django.template.loader import render_to_string
from django.contrib import messages
from star_ratings.models import Rating
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.core.files.base import ContentFile

from editors.models import (
    Editors, Qualification, Experience, ResearchInterest, Quotation, PaperRequest,
    Verification, PaymentMethod, FinancialActivity, StripeProfile, Discipline, RatingFeedback, Question, Answer, Test, TestQuestion, PaperReview, ProfileViewer
)
from editors.forms import (
    EditorFormStep1, QualificationForm, ExperienceForm, QuoteForm, ShortQuoteForm, PaperForm, ShortPaperForm, EditEditorForm,
    CompletePaperForm, AddToCartForm, PaperPriceForm, GetPaidForm, TestForm
)
from message.forms import MessageForm
from editors.utils import create_username, get_charge_price, get_transfer_price, get_service_fee, calculate_rating_stars
from editors.encryption import encrypt_aes
from eseditor.utils import SendEmail
from students.models import Students, StudentFinancialActivity
from students.views import create_student_profile
from message.models import Message

stripe.api_key = settings.STRIPE_SECRET_KEY


def get_editors(query=None, research_method=None, disciplines=None, degree_list=None, interests=None, optional_research_methods=None, rating=None):
    editors = Editors.objects.filter(is_verified=True)

    if query:
        query_list = query.split(' ')

        for q in query_list:
            q = q.strip()
            editors = editors.filter(
                Q(user__first_name__icontains=q) |
                Q(user__last_name__icontains=q)
            )

    if research_method:
        if 'Others' in research_method:
            editors = editors.filter(research_methods="Others")
            if optional_research_methods:
                editors = editors.filter(optional_research_methods__icontains=optional_research_methods)
        elif 'Qualitative' in research_method and 'Quantitative' in research_method:
            editors = editors.filter(research_methods="Both")
        elif 'Quantitative' in research_method:
            editors = editors.filter(research_methods="Quantitative")
        elif 'Qualitative' in research_method:
            editors = editors.filter(research_methods="Qualitative")

    if degree_list:
        editor_ids = Qualification.objects.filter(degree_level__in=degree_list).values_list("editor__id", flat=True)
        editors = editors.filter(id__in=editor_ids)

    if interests:
        interests = ResearchInterest.objects.filter(interest__in=interests)
        editors = editors.filter(research_interest__in=interests).distinct()

    if disciplines:
        disciplines = Discipline.objects.filter(name__in=disciplines)
        editors = editors.filter(discipline__in=disciplines).distinct()

    if rating:
        rating_filter = float(rating)
        editors= editors.exclude(rating_star__lt=rating_filter)

    editors = editors.order_by('-rating_star')

    return editors


def editing_services(request):
    top_degree_list = Qualification.objects.exclude(degree_level__isnull=True).values('degree_level').annotate(degree_count=Count('degree_level')).order_by('-degree_count')[:5]

    research_interest_list = ResearchInterest.objects.all().values_list("interest", flat=True)

    discipline_list = Discipline.objects.all().values_list("name", flat=True)

    return render_to_response("editors/editing_services.html", {
        'current': 'editing_services',
        'top_degree_list': top_degree_list,
        'research_interest_list': research_interest_list,
        'discipline_list': discipline_list
    }, context_instance=RequestContext(request))


def editor_home(request):
    query = request.GET.get('q', '')
    research_method = request.GET.getlist('research_method', '')
    rating = request.GET.get('rating', '')
    degree_list = request.GET.getlist('degree', '')
    interests = request.GET.get('interests', '')
    disciplines = request.GET.getlist('disciplines', '')

    if interests:
        interests = interests.split(",")

    optional_research_methods = request.GET.get('optional_research_methods', '')

    top_degree_list = Qualification.objects.exclude(degree_level__isnull=True).values('degree_level').annotate(degree_count=Count('degree_level')).order_by('-degree_count')[:5]

    research_interest_list = ResearchInterest.objects.all().values_list("interest", flat=True)

    discipline_list = Discipline.objects.all().values_list("name", flat=True)

    if request.user.is_authenticated():    
        save_profile_viewer = ProfileViewer.objects.filter(user=request.user).order_by('-modified')[:10]
    else:
        save_profile_viewer = []

    no_result = False
    editors = get_editors(query, research_method, disciplines, degree_list, interests, optional_research_methods, rating)

    if not editors:
        editors = get_editors()
        no_result = True

    if len(editors) > 10:
        editors = editors[:10]
        more = True
    else:
        more = False

    context = {
        "start": 10,
        "save_profile_viewer": save_profile_viewer,
        "more": more,
        "editors": editors,
        "rating": rating,
        "top_degree_list": top_degree_list,
        "degree_list": degree_list,
        "discipline_list": discipline_list,
        "disciplines": disciplines,
        "current": "editor_home",
        "research_interset_list": research_interest_list,
        "research_interest": interests,
        "research_method": research_method,
        "query": query,
        "no_result": no_result,
        "optional_research_methods": optional_research_methods
    }

    return render_to_response("editors/home.html", context ,context_instance=RequestContext(request))


def load_more_editors(request):
    start = int(request.GET.get('start'))
    query = request.GET.get('q', '')
    research_method = request.GET.getlist('research_method', '')
    rating = request.GET.get('rating', '')
    degree_list = request.GET.getlist('degree', '')
    interests = request.GET.get('interests', '')
    optional_research_methods = request.GET.get('optional_research_methods', '')
    disciplines = request.GET.getlist('disciplines', '')

    if interests:
        interests = interests.split(",")

    editors = get_editors(query, research_method, disciplines, degree_list, interests, optional_research_methods, rating)
    if not editors:
        editors = get_editors()

    if len(editors) > start+10:
        editors = editors[start:start+10]
        more = True
    else:
        editors = editors[start:]
        more = False

    context = {"editors": editors}

    template = render_to_string("editors/load_more_editors.html", context ,context_instance=RequestContext(request))

    response = {"start": start+10, "more": more, "data": template}

    return HttpResponse(json.dumps(response), content_type="applicaton/json")


def create_editor_step1(request):
    if request.user.is_authenticated():
        return redirect("/")   

    if request.method == "POST":
        form = EditorFormStep1(request.POST, request.FILES)

        if form.is_valid():
            user = User.objects.create(
                username=create_username('%s %s' % (form.cleaned_data["first_name"], form.cleaned_data["last_name"])),
                first_name=form.cleaned_data["first_name"],
                last_name=form.cleaned_data["last_name"],
                email=form.cleaned_data["email"],
                is_active=False
            )

            editor = Editors.objects.create(
                user=user,
                phone_no=form.cleaned_data["phone_no"],
                research_methods=form.cleaned_data["research_methods"],
                optional_research_methods=form.cleaned_data["optional_research_methods"],
                referencing_style=form.cleaned_data["referencing_style"],
                optional_referencing_style=form.cleaned_data["optional_referencing_style"],
                street1=form.cleaned_data["street1"],
                street2=form.cleaned_data["street2"],
                city=form.cleaned_data["city"],
                state=form.cleaned_data["state"],
                postal_code=form.cleaned_data["postal_code"],
                country=form.cleaned_data["country"],
                nationality=form.cleaned_data["nationality"],
                bio=form.cleaned_data["bio"],
            )

            string = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()?"
            password = "".join(random.sample(string, 8))

            user.set_password(password)
            user.save()

            msg = SendEmail()
            msg.send(recipient=[editor.user.email], template_path='email_messages/new_editor_email.html',
                context={
                    'editor': editor,
                    'password' : password
                },
                subject='Welcome to EducationStudio!'
            )

            default_avatar = form.cleaned_data["default_avatar"]

            if default_avatar:
                default_avatar = default_avatar[1:]
                avatar_path = os.path.join(settings.BASE_DIR, default_avatar)
                filename = default_avatar.split("/")[-1]
                with open(avatar_path, 'r') as f:
                    editor.avatar.save(filename, ContentFile(f.read()))
                f.close()

            if form.cleaned_data["discipline"]:
                discipline_list = form.cleaned_data["discipline"]
                for discipline in discipline_list:
                    try:
                        discipline_obj = Discipline.objects.get(id=discipline)
                        editor.discipline.add(discipline_obj)
                    except Discipline.DoesNotExist:
                        pass


            if form.cleaned_data["research_interest"]:
                interest_list = form.cleaned_data["research_interest"].split(",")
                for interest in interest_list:
                    try:
                        interest = ResearchInterest.objects.get(interest__iexact=interest)
                    except ResearchInterest.DoesNotExist:
                        interest = interest.strip()
                        if interest:
                            interest = ResearchInterest.objects.create(interest=interest)

                    editor.research_interest.add(interest)

            if request.FILES:
                default_avatar = request.POST.get('default_avatar', '')

                avatar = request.FILES.get("avatar")
                identity_document = request.FILES.get("identity_document")
                qualification_document = request.FILES.get("qualification_document")
                address_document = request.FILES.get("address_document")
                editor.avatar = avatar
                editor.identity_document = identity_document
                editor.qualification_document = qualification_document
                editor.address_document = address_document

            editor.save()

            if editor.research_methods != "Others":
                editor.optional_research_methods = ""

            editor.save()

            request.session['new_editor'] = editor.id

            admin_emails = User.objects.filter(is_superuser=True).values_list('email', flat=True)

            msg = SendEmail(request)
            msg.send(recipient=admin_emails, template_path='email_messages/new_editor_application.html',
                context={
                    'editor_link': settings.SITE_URL+"/admin/editors/editors/{}".format(editor.id),
                    'editor': editor
                },
                subject='New Editor Applied!'
            )

            return redirect(reverse("create_editor_step2"))

        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                print tmp    
            
    else:
        form = EditorFormStep1()

    return render_to_response("editors/create_editor_step1.html", {
        "form": form,
        "current": "signup"
    }, context_instance=RequestContext(request))


def get_research_interest(request):
    """
    Function is used to get the skills.
    """

    response_data = []

    if request.is_ajax():
        interests = ResearchInterest.objects.filter(interest__icontains=request.GET.get('q'))

        for interest in interests:
            response_data.append(interest.interest)

    return HttpResponse(json.dumps(response_data), "application/json")


def create_editor_step2(request):
    if not 'new_editor' in request.session:
        return redirect(reverse('create_editor_step1'))
    else:    
        editor = Editors.objects.get(id=request.session['new_editor'])

    return render_to_response("editors/create_editor_step2.html", {
        "current": "test",
        
    }, context_instance=RequestContext(request))   


def create_test(request):
    if not 'new_editor' in request.session:
        return redirect(reverse('create_editor_step1'))
    else:
        editor = Editors.objects.get(id=request.session['new_editor'])

    test = Test.objects.create(
        editor = editor,
        completed = False
    )
    return redirect(reverse('start_test', kwargs={'test_id': test.id}))


def start_test(request, test_id):
    if not 'new_editor' in request.session:
        return redirect(reverse('create_editor_step1'))
    else:
        editor = Editors.objects.get(id=request.session['new_editor'])

    test = get_object_or_404(Test, id=test_id)

    if test.completed == True:
        return redirect(reverse('create_editor_step3'))


    start_time = test.created
    end_time = test.created + timedelta(minutes=settings.QUIZ_TIME)
    pending_time = end_time - timezone.now()
    hours, remainder = divmod(pending_time.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)

    if TestQuestion.objects.filter(test=test).count() == 0:
        questions = []
        questions.extend(Question.objects.filter(difficulty_level="EASY").order_by('?')[:settings.NO_EASY_QUESTION])
        questions.extend(Question.objects.filter(difficulty_level="MEDIUM").order_by('?')[:settings.NO_MEDIUM_QUESTION])
        questions.extend(Question.objects.filter(difficulty_level="DIFFICULT").order_by('?')[:settings.NO_HARD_QUESTION])
        shuffle(questions)

        for ques in questions:
            TestQuestion.objects.create(test=test, question=ques)

    if not TestQuestion.objects.filter(test=test, is_attempted=False).exists() or (minutes > 30 or hours > 0): 
        return redirect(reverse("test_completed", kwargs={'test_id': test.id}))

    if request.method == "POST":
        question_id = request.POST["question_id"]
        random_question = Question.objects.get(id=question_id)
        form = TestForm(request.POST, question=random_question)

        if form.is_valid():
            test_question = TestQuestion.objects.get(test=test, question=random_question)
            test_question.is_attempted = True
            test_question.answer = form.cleaned_data["answer"]
            test_question.save()

            if test_question.answer.is_correct:
                test.score = test.score + 1
                test.save()

            return redirect(reverse('start_test', kwargs={'test_id': test.id}))
    else:
        random_question = TestQuestion.objects.filter(test=test, is_attempted=False)[0]
        counter = TestQuestion.objects.filter(test=test, is_attempted=True).count() + 1
        form = TestForm(question=random_question.question, initial={"question_id": random_question.question.id})

    total_question = TestQuestion.objects.filter(test=test).count()
    if total_question == counter:
        last_question = True
    else:
        last_question = False    
        
    return render_to_response("editors/test.html", {
        "current": "test",
        "hours" : hours,
        "minutes" : minutes,
        "seconds" : seconds,
        "random_question" : random_question.question,
        "counter" : counter,
        "last_question": last_question,
        "total_question": total_question,
        "test": test,
        "form" : form
    }, context_instance=RequestContext(request))


def skip_question(request, question_id, test_id):
    if not 'new_editor' in request.session:
        return redirect(reverse('create_editor_step1'))
    else:    
        editor = Editors.objects.get(id=request.session['new_editor'])

    test = Test.objects.get(id=test_id)
    test_question = TestQuestion.objects.get(test=test, question__id=question_id)
    test_question.is_attempted = True
    test_question.save()
    return redirect(reverse('start_test', kwargs={'test_id': test.id}))


def test_completed(request, test_id):
    if not 'new_editor' in request.session:
        return redirect(reverse('create_editor_step1'))
    else:    
        editor = Editors.objects.get(id=request.session['new_editor'])

    test = Test.objects.get(id=test_id)

    test.completed = True
    test.save()

    if test.score < 1:
        del request.session["new_editor"]
        test_pass = False

    else:
        test_pass = True    

    return render_to_response("editors/test_completed.html", {
        "test_pass": test_pass,                      
        "score": test.score,
        "test" : test,
        "current": "test"
    }, context_instance=RequestContext(request))


def create_editor_step3(request):
    if not 'new_editor' in request.session:
        return redirect(reverse('create_editor_step1'))
    else:
        editor = Editors.objects.get(id=request.session['new_editor'])

    test = Test.objects.get(editor=editor)
    if test.completed == False:
        return redirect(reverse('start_test', kwargs={'test_id': test.id}))

    qualification_detail_content = []
    for qualification in editor.qualification_set.all():
        qualification_detail_content.append(
            render_to_string('editors/qualification_detail.html', {
                'qualification': qualification
            }, context_instance=RequestContext(request))
        )

    experience_detail_content = []
    for experience in editor.experience_set.all():
        experience_detail_content.append(
            render_to_string('editors/experience_detail.html', {
                'experience': experience
            }, context_instance=RequestContext(request))
        )

    qualification_form_content = render_to_string('editors/qualification_form.html', {
        'form': QualificationForm()
    }, context_instance=RequestContext(request))

    experience_form_content = render_to_string('editors/experience_form.html', {
        'form': ExperienceForm()
    }, context_instance=RequestContext(request))

    return render_to_response("editors/create_editor_step3.html", {
        "current": "signup",
        "qualification_detail_content": qualification_detail_content,
        "experience_detail_content": experience_detail_content,
        "qualification_form_content": qualification_form_content,
        "experience_form_content": experience_form_content
    }, context_instance=RequestContext(request))


def create_editor_step4(request):
    if "new_editor" in request.session:
        editor = Editors.objects.get(id=request.session['new_editor'])
        user = User.objects.get(editors=editor)
        user.is_active = True
        user.save()

        if editor and not request.user.is_authenticated():
            backend = get_backends()[0]
            editor.user.backend = "%s.%s" % (backend.__module__, backend.__class__.__name__)
            login(request, editor.user)
            messages.add_message(request, messages.INFO, "Thanks you for applying to join Education Studio team. We are currently reviewing and approving your application. You'll hear from us soon.")
            return redirect(reverse('profile', kwargs={'username': editor.user.username}))

    else:
        return redirect(reverse("create_editor_step3"))


def show_qualification(request):
    response = {"status": False, "errors": []}

    qualification_id = request.GET.get('qualification_id', '')

    qualification = Qualification.objects.get(id=qualification_id)

    response["status"] = True
    response["qualification"] = render_to_string('editors/qualification_detail.html', {
        'qualification': qualification
    }, context_instance=RequestContext(request))

    return HttpResponse(json.dumps(response))


def show_experience(request):
    response = {"status": False, "errors": []}

    experience_id = request.GET.get('experience_id', '')

    experience = Experience.objects.get(id=experience_id)

    response["status"] = True
    response["experience"] = render_to_string('editors/experience_detail.html', {
        'experience': experience
    }, context_instance=RequestContext(request))

    return HttpResponse(json.dumps(response))


def get_qualification_form(request):
    if "new_editor" in request.session:
        editor = request.session["new_editor"]
    else:
        editor = Editors.objects.get(user=request.user)

    qualification_id = request.GET.get('qualification_id', '')

    if qualification_id:
        qualification = Qualification.objects.get(id=qualification_id)

        qualifications = Qualification.objects.exclude(id=qualification.id).filter(editor=editor)
        qualification_detail_content = []
        for qualification_obj in qualifications:
            qualification_detail_content.append(
                render_to_string('editors/qualification_detail.html', {
                    'qualification': qualification_obj
                }, context_instance=RequestContext(request))
            )

        form = QualificationForm(initial={
            'degree' : qualification.degree,
            'degree_level' : qualification.degree_level,
            'university' : qualification.university,
            'from_month' : qualification.from_month,
            'from_year' : qualification.from_year,
            'to_month' : qualification.to_month,
            'to_year' : qualification.to_year,
        })
        qualification_form_content = render_to_string('editors/qualification_form.html', {
            'form': form,
            'qualification_id': qualification_id,
            'qualification_detail_content': qualification_detail_content
        }, context_instance=RequestContext(request))
    else:
        qualifications = Qualification.objects.filter(editor=editor)
        qualification_detail_content = []
        for qualification_obj in qualifications:
            qualification_detail_content.append(
                render_to_string('editors/qualification_detail.html', {
                    'qualification': qualification_obj
                }, context_instance=RequestContext(request))
            )

        qualification_form_content = render_to_string('editors/qualification_form.html', {
            'form': QualificationForm(),
            'qualification_detail_content': qualification_detail_content
        }, context_instance=RequestContext(request))

    response = {
        "data": qualification_form_content,
        "status": True
    }

    return HttpResponse(json.dumps(response))


def get_experience_form(request):
    if "new_editor" in request.session:
        editor = request.session["new_editor"]
    else:
        editor = Editors.objects.get(user=request.user)

    experience_id = request.GET.get('experience_id', '')

    if experience_id:
        experience = Experience.objects.get(id=experience_id)

        experiences = Experience.objects.exclude(id=experience.id).filter(editor=editor)
        experience_detail_content = []
        for experience_obj in experiences:
            experience_detail_content.append(
                render_to_string('editors/experience_detail.html',{
                    'experience' : experience_obj
                    }, context_instance=RequestContext(request))
            )

        form = ExperienceForm(initial={
            'company_name' : experience.company_name,
            'title' : experience.title,
            'location' : experience.location,
            'description' : experience.description,
            'is_current' : experience.is_current,
            'from_month' : experience.from_month,
            'from_year' : experience.from_year,
            'to_month' : experience.to_month,
            'to_year' : experience.to_year,
        })
        experience_form_content = render_to_string('editors/experience_form.html', {
            'form': form,
            'experience_id' : experience_id,
            'experience_detail_content' : experience_detail_content
        }, context_instance=RequestContext(request))
    else:
        experiences = Experience.objects.filter(editor=editor)
        experience_detail_content = []
        for experience_obj in experiences:
            experience_detail_content.append(
                render_to_string('editors/experience_detail.html',{
                    'experience' : experience_obj
                },context_instance=RequestContext(request))
            )

        experience_form_content = render_to_string('editors/experience_form.html', {
            'form': ExperienceForm(),
            'experience_detail_content': experience_detail_content
        }, context_instance=RequestContext(request))

    response = {
        "data": experience_form_content,
        "status": True
    }

    return HttpResponse(json.dumps(response))


def save_qualification_form(request):
    response = {"status": False, "errors": []}

    if not request.user.is_authenticated():
        if not 'new_editor' in request.session:
            return HttpResponse(json.dumps(response))
        else:
            editor = Editors.objects.get(id=request.session['new_editor'])
    else:
        editor = Editors.objects.get(user=request.user)

    if request.method == "POST":
        form = QualificationForm(request.POST, request.FILES)

        if form.is_valid():
            qualification, created = Qualification.objects.get_or_create(
                editor=editor,
                degree=form.cleaned_data["degree"],
                degree_level=form.cleaned_data["degree_level"],
                university=form.cleaned_data["university"]
            )
            qualification.degree_level = form.cleaned_data["degree_level"]
            qualification.from_month = form.cleaned_data["from_month"]
            qualification.from_year = form.cleaned_data["from_year"]
            qualification.to_month = form.cleaned_data["to_month"]
            qualification.to_year = form.cleaned_data["to_year"]

            if request.FILES:
                qualification.qualification_document = request.FILES["qualification_document"]

            qualification.save()

            response["status"] = True
            response["qualification"] = qualification_form_content = render_to_string('editors/qualification_detail.html', {
                'qualification': qualification
            }, context_instance=RequestContext(request))
        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                response['errors'].append(tmp)

    return HttpResponse(json.dumps(response))


def save_experience_form(request):
    response = {"status": False, "errors": []}

    if not request.user.is_authenticated():
        if not 'new_editor' in request.session:
            return HttpResponse(json.dumps(response))
        else:
            editor = Editors.objects.get(id=request.session['new_editor'])
    else:
        editor = Editors.objects.get(user=request.user)

    if request.method == "POST":
        form = ExperienceForm(request.POST)

        if form.is_valid():
            experience, created = Experience.objects.get_or_create(
                editor=editor,
                company_name=form.cleaned_data["company_name"]
            )

            experience.title = form.cleaned_data["title"]
            experience.location = form.cleaned_data["location"]
            experience.description = form.cleaned_data["description"]
            experience.is_current = form.cleaned_data["is_current"]
            experience.from_month = form.cleaned_data["from_month"]
            experience.from_year = form.cleaned_data["from_year"]
            experience.to_month = form.cleaned_data["to_month"]
            experience.to_year = form.cleaned_data["to_year"]
            experience.save()

            response["status"] = True
            response["experience"] = experience_form_content = render_to_string('editors/experience_detail.html', {
                'experience': experience
            }, context_instance=RequestContext(request))
        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                response['errors'].append(tmp)

    return HttpResponse(json.dumps(response))


def edit_qualification_form(request, qualification_id):
    response = {"status": False, "errors": []}

    if not request.user.is_authenticated():
        if not 'new_editor' in request.session:
            return HttpResponse(json.dumps(response))
        else:
            editor = Editors.objects.get(id=request.session['new_editor'])
    else:
        editor = Editors.objects.get(user=request.user)

    qualification = Qualification.objects.get(id=qualification_id)

    if request.method == "POST":
        form = QualificationForm(request.POST, request.FILES)

        if form.is_valid():
            qualification.degree = form.cleaned_data["degree"]
            qualification.degree_level = form.cleaned_data["degree_level"]
            qualification.university = form.cleaned_data["university"]
            qualification.from_month = form.cleaned_data["from_month"]
            qualification.from_year = form.cleaned_data["from_year"]
            qualification.to_month = form.cleaned_data["to_month"]
            qualification.to_year = form.cleaned_data["to_year"]

            if request.FILES:
                qualification.qualification_document = request.FILES["qualification_document"]

            qualification.save()

            response["status"] = True
            response["qualification"] = render_to_string('editors/qualification_detail.html', {
                'qualification': qualification
            }, context_instance=RequestContext(request))
        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                response['errors'].append(tmp)

    return HttpResponse(json.dumps(response))


def edit_experience_form(request, experience_id):
    response = {"status": False, "errors": []}

    if not request.user.is_authenticated():
        if not 'new_editor' in request.session:
            return HttpResponse(json.dumps(response))
        else:
            editor = Editors.objects.get(id=request.session['new_editor'])
    else:
        editor = Editors.objects.get(user=request.user)

    experience = Experience.objects.get(id=experience_id)

    if request.method == "POST":
        form = ExperienceForm(request.POST)

        if form.is_valid():
            experience.company_name = form.cleaned_data["company_name"]
            experience.title = form.cleaned_data["title"]
            experience.location = form.cleaned_data["location"]
            experience.description = form.cleaned_data["description"]
            experience.is_current = form.cleaned_data["is_current"]
            experience.from_month = form.cleaned_data["from_month"]
            experience.from_year = form.cleaned_data["from_year"]
            experience.to_month = form.cleaned_data["to_month"]
            experience.to_year = form.cleaned_data["to_year"]
            experience.save()

            response["status"] = True
            response["experience"] = render_to_string('editors/experience_detail.html', {
                'experience': experience
            }, context_instance=RequestContext(request))
        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                response['errors'].append(tmp)

    return HttpResponse(json.dumps(response))


def delete_qualification(request):
    response = {"status": False, "errors": []}
    qualification_id = request.GET.get('qualification_id', '')


    if not request.user.is_authenticated():
        if not 'new_editor' in request.session:
            return HttpResponse(json.dumps(response))
        else:
            editor = Editors.objects.get(id=request.session['new_editor'])
    else:
        editor = Editors.objects.get(user=request.user)

    qualification = Qualification.objects.get(id=qualification_id)

    if qualification.editor == editor:
        qualification.delete()

    response["status"] = True
    return HttpResponse(json.dumps(response))


def delete_experience(request):
    response = {"status": False, "errors": []}
    experience_id = request.GET.get('experience_id', '')

    if not request.user.is_authenticated():
        if not 'new_editor' in request.session:
            return HttpResponse(json.dumps(response))
        else:
            editor = Editors.objects.get(id=request.session['new_editor'])
    else:
        editor = Editors.objects.get(user=request.user)

    experience = Experience.objects.get(id=experience_id)

    if experience.editor == editor:
        experience.delete()

    response["status"] = True
    return HttpResponse(json.dumps(response))


def profile(request, username):
    """
    function is used to provide the profile page view of an editor
    """
    content_type = ContentType.objects.get_for_model(PaperRequest)

    editor = Editors.objects.get(user__username=username)

    new_requests = PaperRequest.objects.filter(editor=editor).filter(status__in=['Pending', 'Progress']).order_by('-created')

    quotes = Quotation.objects.filter(editor=editor).order_by('-created')

    papers = PaperRequest.objects.filter(editor=editor, status__in=['Rejected', 'Ready To Review', 'Approved', 'Completed']).order_by('-created')

    if not request.user == editor.user and request.user.is_authenticated():
        save_profile_viewer, created = ProfileViewer.objects.get_or_create(
            user = request.user,
            editor = Editors.objects.get(user__username=username)
        )
        if not created:
            save_profile_viewer.save()

    if request.user.is_authenticated():    
        save_profile_viewer = ProfileViewer.objects.filter(user=request.user).order_by('-modified')[:10]
    else:
        save_profile_viewer = []        

    score = 0.0
    has_rating = False

    paper_ids = papers.values_list('id', flat=True)
    ratings = Rating.objects.filter(content_type=content_type, object_id__in=paper_ids)
    for rating in ratings:
        score += float(rating.average)

    if ratings:
        score = score/(len(ratings))
        has_rating = True

    score = score
    star = int(score)
    semi_star = not score.is_integer()
    star_empty = 4 - int(score) if semi_star else 5 - int(score)

    query = request.GET.get('q', '')
    if query:
        papers = papers.filter(title__icontains=query)

    stripe_profile = None
    if request.user == editor.user:
        try:
            stripe_profile = StripeProfile.objects.get(user=request.user)
        except StripeProfile.DoesNotExist:
            pass

    if len(papers) > 10:
        papers = papers[:10]
        more = True
    else:
        more = False

    if len(new_requests) > 10:
        new_requests = new_requests[:10]
        more1 = True
    else:
        more1 = False

    if len(quotes) > 10:
        quotes = quotes[:10]
        more2 = True
    else:
        more2 = False                 

    context = {
        "start": 10,
        "more": more,
        "start1": 10,
        "more1": more1,
        "start2": 10,
        "more2": more2,
        'current': 'profile',
        "save_profile_viewer" :save_profile_viewer,
        'star': range(star),
        'semi_star': semi_star,
        'star_empty': range(star_empty),
        'has_rating': has_rating,
        'editor': editor,
        'papers': papers,
        'new_requests': new_requests,
        'quotes' : quotes,
        'currency': settings.PLATFORM_CURRENCY_DISPLAY,
        'query': query,
        'stripe_profile': stripe_profile,
        'quote_form': QuoteForm(initial={"editor_id": editor.id}),
        'paper_form': PaperForm(initial={"editor_id": editor.id}),
        "complete_paper_form": CompletePaperForm(),
        "paper_price_form": PaperPriceForm(),
        "add_to_cart_form": AddToCartForm()
    }

    if request.user.is_authenticated():
        context['quote_form'] = ShortQuoteForm(initial={"editor_id": editor.id})
        context['paper_form'] = ShortPaperForm(initial={"editor_id": editor.id})

    return render_to_response("editors/profile.html", context, context_instance=RequestContext(request))


def load_more_edited_paper(request):
    start = int(request.GET.get('start'))

    username = request.GET.get('username')

    editor = Editors.objects.get(user__username=username)

    papers = PaperRequest.objects.filter(editor=editor, status__in=['Rejected', 'Ready To Review', 'Approved', 'Completed']).order_by('-created')
    
    if len(papers) > start+10:
        papers = papers[start:start+10]
        more = True
    else:
        papers = papers[start:]
        more = False

    context = {"papers": papers, "start":start}

    template = render_to_string("editors/load_more_edited_paper.html", context ,context_instance=RequestContext(request))

    response = {"start": start+10, "more": more, "data": template}

    return HttpResponse(json.dumps(response), content_type="applicaton/json")


def load_more_paper_request(request):
    start1 = int(request.GET.get('start1'))

    username = request.GET.get('username')

    editor = Editors.objects.get(user__username=username)

    new_requests = PaperRequest.objects.filter(editor=editor).filter(status__in=['Pending', 'Progress']).order_by('-created')
    
    if len(new_requests) > start1+10:
        new_requests = new_requests[start1:start1+10]
        more1 = True
    else:
        new_requests = new_requests[start1:]
        more1 = False

    context = {"new_requests": new_requests, "start":start1}

    template = render_to_string("editors/load_more_paper_request.html", context ,context_instance=RequestContext(request))

    response = {"start1": start1+10, "more1": more1, "data": template}

    return HttpResponse(json.dumps(response), content_type="applicaton/json")


def load_more_quotes_request(request):
    start2 = int(request.GET.get('start2'))

    username = request.GET.get('username')

    editor = Editors.objects.get(user__username=username)

    quotes = Quotation.objects.filter(editor=editor).order_by('-created')
    
    if len(quotes) > start2+10:
        quotes = quotes[start2:start2+10]
        more2 = True
    else:
        quotes = quotes[start2:]
        more2 = False

    context = {"quotes": quotes, "start":start2}

    template = render_to_string("editors/load_more_quotes_request.html", context ,context_instance=RequestContext(request))

    response = {"start2": start2+10, "more2": more2, "data": template}

    return HttpResponse(json.dumps(response), content_type="applicaton/json")        


def quote_request(request):
    if request.is_ajax():
        response = {"status": False, "errors": []}

        if request.method == "POST":
            if request.user.is_authenticated():
                form = ShortQuoteForm(request.POST, request.FILES)
            else:
                form = QuoteForm(request.POST, request.FILES)

            if form.is_valid():
                editor = Editors.objects.get(id=form.cleaned_data["editor_id"])

                if request.user.is_authenticated():
                    try:
                        student = Students.objects.get(user=request.user)
                    except:
                        messages.add_message(request, messages.ERROR, "You are not allowed to take this action!")
                        response['next'] = '/editors/%s/' % editor.user.username
                        response["status"] = True
                        return HttpResponse(json.dumps(response))
                else:
                    email = form.cleaned_data['email']
                    if email and User.objects.filter(email__iexact=email).exists():
                        messages.add_message(request, messages.ERROR, "This email is already registered, please login to continue!")
                        response['redirect_to'] = '/accounts/login/?next=/editors/%s/' % editor.user.username
                        response["status"] = False
                        return HttpResponse(json.dumps(response))
                    else:
                        first_name = form.cleaned_data['first_name']
                        last_name = form.cleaned_data['last_name']
                        student = create_student_profile(email, first_name, last_name)

                if student and not request.user.is_authenticated():
                    backend = get_backends()[0]
                    student.user.backend = "%s.%s" % (backend.__module__, backend.__class__.__name__)
                    login(request, student.user)

                discipline = Discipline.objects.get(name=form.cleaned_data['discipline'])

                quotation = Quotation.objects.create(
                    editor=editor,
                    student=student,
                    title=form.cleaned_data['title'],
                    discipline = discipline,
                    description = form.cleaned_data['description'],
                    words_count=form.cleaned_data['words_count']
                )
                if request.FILES:
                    quotation.document = request.FILES['document']
                    quotation.save()

                message_data = render_to_string('msgs/quote_request.html', {'quotation': quotation}, RequestContext(request))
                message = Message.objects.create(
                    sender=student.user,
                    recipient=editor.user,
                    subject="Quotation Request from %s - %s" % (quotation.student.user.get_full_name(), quotation.title),
                    message=message_data
                )

                msg = SendEmail(request)
                msg.send(recipient=[editor.user.email], template_path='email_messages/msg_notification.html',
                    context={
                        'msg_link': settings.SITE_URL+reverse("message_details", kwargs={"msg_id": message.id}),
                        'recipient_name': editor.user.first_name
                    },
                    subject='[Education Studio] New message received!'
                )

                messages.add_message(request, messages.INFO, "Hi {}, Your Quote request has been successfully sent to {}.".format(student.user.first_name, editor.user.first_name))

                response["status"] = True

            else:
                for key, value in form.errors.items():
                    tmp = {'key': key, 'error': value.as_text()}
                    response['errors'].append(tmp)

            return HttpResponse(json.dumps(response))

    return redirect("/")


def paper_request(request):
    if request.is_ajax():
        response = {"status": False, "errors": []}

        if request.method == "POST":
            if request.user.is_authenticated():
                form = ShortPaperForm(request.POST, request.FILES)
            else:
                form = PaperForm(request.POST, request.FILES)

            if form.is_valid():
                editor = Editors.objects.get(id=form.cleaned_data["editor_id"])

                if request.user.is_authenticated():
                    try:
                        student = Students.objects.get(user=request.user)
                    except:
                        messages.add_message(request, messages.ERROR, "You are not allowed to take this action!")
                        response['next'] = '/editors/%s/' % editor.user.username
                        response["status"] = True
                        return HttpResponse(json.dumps(response))
                else:
                    email = form.cleaned_data['email']
                    if email and User.objects.filter(email__iexact=email).exists():
                        messages.add_message(request, messages.ERROR, "This email is already registered, please login to continue!")
                        response['redirect_to'] = '/accounts/login/?next=/editors/%s/' % editor.user.username
                        response["status"] = False
                        return HttpResponse(json.dumps(response))
                    else:
                        first_name = form.cleaned_data['first_name']
                        last_name = form.cleaned_data['last_name']
                        student = create_student_profile(email, first_name, last_name)

                if student and not request.user.is_authenticated():
                    backend = get_backends()[0]
                    student.user.backend = "%s.%s" % (backend.__module__, backend.__class__.__name__)
                    login(request, student.user)

                discipline = Discipline.objects.get(name=form.cleaned_data['discipline'])

                paper_request = PaperRequest.objects.create(
                    editor=editor,
                    student=student,
                    title=form.cleaned_data['title'],
                    discipline = discipline,
                    price=form.cleaned_data['price'],
                    description=form.cleaned_data['description'],
                    words_count=form.cleaned_data['words_count'],
                    deadline=form.cleaned_data['deadline']
                )

                paper_request.raw_paper = request.FILES['paper']
                paper_request.save()

                response["next"] = reverse("checkout")+"?paper="+str(paper_request.id)
                response["status"] = True

                messages.add_message(request, messages.INFO, "Hi {}, Please complete the checkout to submit your paper to {}.".format(student.user.first_name, editor.user.first_name))
            else:
                for key, value in form.errors.items():
                    tmp = {'key': key, 'error': value.as_text()}
                    response['errors'].append(tmp)

            return HttpResponse(json.dumps(response))

    return redirect("/")


@login_required(login_url=settings.LOGIN_URL)
def paper_detail(request, paper_id):
    paper = PaperRequest.objects.get(id=paper_id)
    reviewed_papers = PaperReview.objects.filter(paper=paper).order_by('-created')  
    editor = Editors.objects.get(id=paper.editor.id)
    student = Students.objects.get(id=paper.student.id)
    msgs = Message.objects.get(paper_request=paper)
    child_msg = msgs.parent.all()
    child_msg.filter(recipient=request.user).update(recipient_read=True)
    child_msg.filter(sender=request.user).update(sender_read=True)

    if msgs.sender == request.user:
        recipient = msgs.recipient
    else:
        recipient = msgs.sender

    if request.method == "POST":
        form = MessageForm(request.POST)

        if form.is_valid():
            msgs = Message.objects.get(id=msgs.id)

            message = form.cleaned_data['message']

            new_msg_obj = form.save()

            msg = SendEmail(request)
            msg.send(recipient=[new_msg_obj.recipient.email], template_path='email_messages/msg_notification.html',
                context={
                    'msg_link': settings.SITE_URL+reverse("message_details", kwargs={"msg_id": msgs.id}),
                    'recipient_name': new_msg_obj.recipient.first_name,
                    'new_msg_obj': new_msg_obj
                },
                subject='[Education Studio] New message received!'
            )
        return redirect(reverse("paper_detail", kwargs={'paper_id': paper.id}))
        
    else:
        form = MessageForm(initial={
            "sender": request.user.id,
            "recipient": recipient.id,
            "subject": msgs.subject,
            "parent_msg": msgs.parent_msg.id if msgs.parent_msg else msgs.id
        })
        
        if paper.editor.user == request.user or paper.student.user == request.user:
            return render_to_response("editors/paper_detail.html", {
                "current": "job_page",
                "reviewed_papers": reviewed_papers,
                "paper": paper,
                "form": form,
                "msgs" : msgs,
                "child_msg" : child_msg,
                "complete_paper_form": CompletePaperForm(),
                'currency': settings.PLATFORM_CURRENCY_DISPLAY,
            }, context_instance=RequestContext(request))
        else:
            return redirect(reverse('profile', kwargs={'username': editor.user.username}))
        

@login_required(login_url=settings.LOGIN_URL)
def edit_editor(request):

    editor = Editors.objects.get(user=request.user)

    if request.method == "POST":
        form = EditEditorForm(request.POST, request.FILES, instance=editor)

        if form.is_valid():
            obj = form.save(commit=False)
            obj.user.first_name = form.cleaned_data['first_name']
            obj.user.last_name = form.cleaned_data['last_name']

            obj.user.save()
            obj.save()

            if obj.research_methods != "Others":
                obj.optional_research_methods = ""
                obj.save()

            default_avatar = form.cleaned_data["default_avatar"]

            if default_avatar:
                default_avatar = default_avatar[1:]
                avatar_path = os.path.join(settings.BASE_DIR, default_avatar)
                filename = default_avatar.split("/")[-1]
                with open(avatar_path, 'r') as f:
                    editor.avatar.save(filename, ContentFile(f.read()))
                f.close()

            editor.research_interest.clear()
            if form.cleaned_data["research_interest"]:
                interest_list = form.cleaned_data["research_interest"].split(",")
                for interest in interest_list:
                    try:
                        interest = ResearchInterest.objects.get(interest__iexact=interest)
                    except ResearchInterest.DoesNotExist:
                        interest = interest.strip()
                        if interest:
                            interest = ResearchInterest.objects.create(interest=interest)

                    editor.research_interest.add(interest)

            editor.discipline.clear()
            if form.cleaned_data["discipline"]:
                discipline_list = form.cleaned_data["discipline"]
                for discipline in discipline_list:
                    try:
                        discipline_obj = Discipline.objects.get(id=discipline)
                        editor.discipline.add(discipline_obj)
                    except Discipline.DoesNotExist:
                        pass


            messages.add_message(request, messages.INFO, "Hi {}, Your profile has been updated successfully.".format(editor.user.first_name))

            return redirect(reverse('profile', kwargs={'username': editor.user.username}))

        return render_to_response("editors/edit_editor.html", {
            "form": form,
            "editor": editor,
        }, context_instance=RequestContext(request))

    else:
        form = EditEditorForm(instance=editor, initial={
            'first_name': editor.user.first_name,
            'last_name': editor.user.last_name,
            'email': editor.user.email,
        })

        qualification_detail_content = []
        for qualification in editor.qualification_set.all():
            qualification_detail_content.append(
                render_to_string('editors/qualification_detail.html', {
                    'qualification': qualification
                }, context_instance=RequestContext(request))
            )

        experience_detail_content = []
        for experience in editor.experience_set.all():
            experience_detail_content.append(
                render_to_string('editors/experience_detail.html', {
                    'experience': experience
                }, context_instance=RequestContext(request))
            )

        return render_to_response("editors/edit_editor.html", {
            "current": "edit",
            "form": form,
            "editor": editor,
            "qualification_detail_content": qualification_detail_content,
            "experience_detail_content" : experience_detail_content
        }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def delete_file(request):
    if request.is_ajax():
        editor = Editors.objects.get(user=request.user)
        type = request.GET.get("type", "")

        setattr(editor, type, "")
        editor.save()

        return HttpResponse(json.dumps({"status": True}))

    return redirect("/")


def delete_qualification_file(request):
    response = {"status": False, "errors": []}
    qualification_id = request.GET.get('qualification_id')

    qualification = Qualification.objects.get(id=qualification_id)

    qualification.qualification_document = ""
    qualification.save()

    response["status"] = True
    return HttpResponse(json.dumps(response))    


@login_required(login_url=settings.LOGIN_URL)
def reject_request(request, paper_id): 
    if request.is_ajax():
        paper = PaperRequest.objects.get(id=paper_id)
        editor = Editors.objects.get(id=paper.editor.id)
        student = Students.objects.get(id=paper.student.id)
        paper.status = "Rejected"
        paper.save()

        msg = SendEmail(request)
        msg.send(recipient=[student.user.email], template_path='email_messages/reject_request.html',
            context={
                "editor": editor,
                "student": student,
                "paper": paper
            },
            subject='Paper Rejected!'
        )
        parent_msg = Message.objects.get(paper_request=paper)

        message_data = render_to_string('msgs/paper_reject.html', {
            "student": student,
            "editor": editor,
            'paper': paper
        }, RequestContext(request))

        message = Message.objects.create(
            sender=paper.editor.user,
            recipient=paper.student.user,
            message=message_data,
            parent_msg=parent_msg
        )

        response = {
            "status": True
        }

        response["message"] = render_to_string("editors/message_alert.html", {
            "class": "danger",
            "message": "Status of {} paper has been updated successfully!".format(paper.title)

        }, context_instance=RequestContext(request))
        return HttpResponse(json.dumps(response))

    return redirect("/")   


@login_required(login_url=settings.LOGIN_URL)
def update_paper_status(request, paper_id):
    if request.is_ajax():
        paper = PaperRequest.objects.get(id=paper_id)
        editor = Editors.objects.get(id=paper.editor.id)
        student = Students.objects.get(id=paper.student.id)
        paper.status = "Progress"
        paper.save()

        msg = SendEmail(request)
        msg.send(recipient=[student.user.email], template_path='email_messages/accept_request.html',
            context={
                "editor": editor,
                "student": student,
                "paper": paper
            },
            subject='Paper Accepted!'
        )
        parent_msg = Message.objects.get(paper_request=paper)

        message_data = render_to_string('msgs/paper_accept.html', {
            "student": student,
            "editor": editor,
            'paper': paper
        }, RequestContext(request))

        message = Message.objects.create(
            sender=paper.editor.user,
            recipient=paper.student.user,
            message=message_data,
            parent_msg=parent_msg
        )
        response = {
            "status": True
        }

        response["message"] = render_to_string("editors/message_alert.html", {
            "class": "info",
            "message": "Status of {} paper has been updated successfully!".format(paper.title)
        }, context_instance=RequestContext(request))

        return HttpResponse(json.dumps(response))

    return redirect("/")


@login_required(login_url=settings.LOGIN_URL)
def paper_ready(request):
    if request.is_ajax():
        response = {
            "status": False,
            "errors": [],
        }

        if request.method == "POST":
            form = CompletePaperForm(request.POST, request.FILES)

            if form.is_valid():
                paper = PaperRequest.objects.get(id=form.cleaned_data['paper_id'])
                paper.status = "Ready To Review"
                paper.edited_paper = request.FILES.get("edited_paper", "")
                paper_review = PaperReview.objects.create(
                    paper=paper,
                    edited_paper=request.FILES["edited_paper"],
                    user = request.user
                )
                paper.save()

                parent_msg = Message.objects.get(paper_request=paper)

                message_data = render_to_string('msgs/paper_complete.html', {
                    'paper': paper,
                }, RequestContext(request))

                message = Message.objects.create(
                    sender=paper.editor.user,
                    recipient=paper.student.user,
                    message=message_data,
                    parent_msg=parent_msg
                )

                msg = SendEmail(request)
                msg.send(recipient=[paper.student.user.email], template_path='email_messages/msg_notification.html',
                    context={
                        'msg_link': settings.SITE_URL+reverse("message_details", kwargs={"msg_id": parent_msg.id}),
                        'recipient_name': paper.student.user.first_name
                    },
                    subject='[Education Studio] New message received!'
                )

                message_data = render_to_string('msgs/paper_download.html', {
                    'paper': paper,
                    'paper_approved': settings.SITE_URL+reverse("paper_approved", kwargs={"paper_id": paper.id})
                }, RequestContext(request))

                message = Message.objects.create(
                    sender=paper.editor.user,
                    recipient=paper.student.user,
                    message=message_data,
                    parent_msg=parent_msg
                )

                msg = SendEmail(request)
                msg.send(recipient=[paper.student.user.email], template_path='email_messages/msg_notification.html',
                    context={
                        'msg_link': settings.SITE_URL+reverse("message_details", kwargs={"msg_id": parent_msg.id}),
                        'recipient_name': paper.student.user.first_name
                    },
                    subject='[Education Studio] New message received!'
                )

                response["status"] = True
                response["next"] = reverse("paper_detail", args=[paper.id])

                messages.add_message(request, messages.INFO, "{} paper has been sent to {} for review!".format(paper.title, paper.student.user.get_full_name()))
            else:
                for key, value in form.errors.items():
                    tmp = {'key': key, 'error': value.as_text()}
                    response['errors'].append(tmp)

            return HttpResponse(json.dumps(response))

        return redirect("/")


def add_to_cart(request):
    if request.is_ajax():
        response = {"status": False, "errors": [], "next": ""}
        if request.method == "POST":
            form = AddToCartForm(request.POST)
            if form.is_valid():
                paper = PaperRequest.objects.get(id=form.cleaned_data["paper_id"])

                token = get_random_string(length=11)
                Verification.objects.create(paper=paper, token=token)

                response["next"] = reverse("paper_download", kwargs={"token": token})
                response["status"] = True
            else:
                for key, value in form.errors.items():
                    tmp = {'key': key, 'error': value.as_text()}
                    response['errors'].append(tmp)

            return HttpResponse(json.dumps(response))

    return redirect("/")


@login_required
def raw_paper_download(request, paper_id):
    paper = PaperRequest.objects.get(id=paper_id)
    extension = paper.raw_paper.name.split(".")[-1]

    file_obj = open(settings.MEDIA_ROOT+paper.raw_paper.name, mode='rb')

    response = HttpResponse(file_obj, content_type='application/{}'.format(extension))
    response['Content-Disposition'] = 'attachment; filename={}.{}'.format(paper, extension)
    
    return response


@login_required
def paper_download(request, paper_id):
    paper = PaperReview.objects.get(id=paper_id)

    extension = paper.edited_paper.name.split(".")[-1]

    file_obj = open(settings.MEDIA_ROOT+paper.edited_paper.name, mode='rb')

    response = HttpResponse(file_obj, content_type='application/{}'.format(extension))
    response['Content-Disposition'] = 'attachment; filename={}.{}'.format(paper, extension)
    
    return response


@login_required
def paper_approved(request, paper_id):
    paper = PaperRequest.objects.get(id=paper_id)

    try:
        student = Students.objects.get(user=request.user)
    except:
        messages.add_message(request, messages.ERROR, "You are not authorized to take this action!")
        return redirect('/')

    if paper.status == 'Approved' or paper.status == 'Completed':
        messages.add_message(request, messages.INFO, "This paper is already approved!")
        return redirect('/')

    if student == paper.student:
        paper.status = "Approved"
        paper.save()

        FinancialActivity.objects.create(
            editor=paper.editor,
            paper=paper,
            description=paper.title,
            amount=paper.price,
            type="Credit"
        )

        parent_msg = Message.objects.get(paper_request=paper)

        message_data = render_to_string('msgs/paper_approved.html', {
            'paper': paper,
            'currency': settings.PLATFORM_CURRENCY_DISPLAY,
            'paper_complete': settings.SITE_URL+reverse("paper_complete", kwargs={"paper_id": paper.id})
        }, RequestContext(request))

        message = Message.objects.create(
            sender=paper.student.user,
            recipient=paper.editor.user,
            message=message_data,
            parent_msg=parent_msg
        )

        msg = SendEmail(request)
        msg.send(recipient=[paper.editor.user.email], template_path='email_messages/msg_notification.html',
            context={
                'msg_link': settings.SITE_URL+reverse("message_details", kwargs={"msg_id": parent_msg.id}),
                'recipient_name': paper.editor.user.first_name
            },
            subject='[Education Studio] New message received!'
        )

        messages.add_message(request, messages.INFO, "Thanks {} for approving this request!".format(paper.student.user.get_full_name()))
    else:
        messages.add_message(request, messages.ERROR, "You are not authorized to approve this paper!")

    return redirect(reverse("paper_detail", args=[paper.id]))


@login_required(login_url=settings.LOGIN_URL)
def paper_complete(request, paper_id):
    paper = PaperRequest.objects.get(id=paper_id)

    try:
        editor = Editors.objects.get(user=request.user)
    except:
        messages.add_message(request, messages.ERROR, "You are not authorized to take this action!")
        return redirect('/')

    if paper.status == "Completed":
        messages.add_message(request, messages.INFO, "This paper is already marked as completed!")
        return redirect('/')

    if editor == paper.editor:
        paper.status = "Completed"
        paper.save()

        parent_msg = Message.objects.get(paper_request=paper)

        message_data = render_to_string('msgs/paper_rating.html', {
            'paper': paper,
            'edited_paper_link': settings.SITE_URL+reverse("profile", kwargs={"username": paper.editor.user.username})+"?q="+paper.title
        }, RequestContext(request))

        message = Message.objects.create(
            sender=paper.editor.user,
            recipient=paper.student.user,
            message=message_data,
            parent_msg=parent_msg
        )

        msg = SendEmail(request)
        msg.send(recipient=[paper.student.user.email], template_path='email_messages/msg_notification.html',
            context={
                'msg_link': settings.SITE_URL+reverse("message_details", kwargs={"msg_id": parent_msg.id}),
                'recipient_name': paper.student.user.first_name
            },
            subject='[Education Studio] New message received!'
        )

        messages.add_message(request, messages.INFO, "This request has been marked completed successfully!")
    else:
        messages.add_message(request, messages.ERROR, "You are not authorized to complete this request!")

    return redirect(reverse("paper_detail", args=[paper.id]))


@login_required
def rating(request):
    if request.is_ajax():
        paper_id = request.GET.get('paper_id', '')

        response = {"status": False, "errors": []}

        paper = PaperRequest.objects.get(id=paper_id)

        response["text"] = render_to_string("editors/rating.html", {
            "paper": paper,
        }, context_instance=RequestContext(request))

        response["paper_id"] = paper_id
        response["status"] = True

        return HttpResponse(json.dumps(response))

    return redirect(reverse("paper_detail", args=[paper.id]))


@login_required
def delete_rating(request):
    if request.is_ajax():
        paper_id = request.GET.get('paper_id', '')

        response = {"status": False, "errors": []}

        paper = PaperRequest.objects.get(id=paper_id)

        content_type = ContentType.objects.get_for_model(PaperRequest)

        papers = PaperRequest.objects.filter(editor=paper.editor, status='Completed').order_by('-created')
        paper_ids = papers.values_list('id', flat=True)
        ratings = Rating.objects.filter(content_type=content_type, object_id__in=paper_ids)

        ratings.delete()

        response["status"] = True

        return HttpResponse(json.dumps(response))

    return redirect(reverse("paper_detail", args=[paper.id]))    


def checkout(request):
    paper_id = request.GET.get("paper", "")
    student = Students.objects.get(user=request.user)

    try:
        paper = PaperRequest.objects.get(id=paper_id)
    except PaperRequest.DoesNotExist:
        messages.add_message(request, messages.ERROR, "This is not a valid paper!")
        return redirect("/")

    if not paper.status == 'Draft':
        messages.add_message(request, messages.ERROR, "This is not a valid paper!")
        return redirect("/")

    total_price = get_charge_price(paper.price)

    if request.method == "POST":
        if request.POST.get("stripeToken", ""):
            stripeToken = request.POST['stripeToken']

            amount = int(total_price*100)

            response = stripe.Charge.create(
                amount = amount,
                currency = settings.PLATFORM_CURRENCY,
                source=stripeToken,
                description='Charge for Paper request from - %s (%s) to editor id - %s' % (paper.student.user.get_full_name(), paper.student.user.email, paper.editor.id)
            )

            paper.status = "Pending"
            paper.charge_id = response.id
            paper.price_paid = amount/100.0
            paper.save()

            StudentFinancialActivity.objects.create(
                student=student,
                paper=paper,
                description=paper.description,
                type="Debit",
                amount=paper.price,
                charge_id=response.id
            )

            StudentFinancialActivity.objects.create(
                student=student,
                paper = paper,
                description="Service Fee",
                type="Debit",
                amount=get_service_fee(paper.price),
                charge_id=response.id
            )

            message_data = render_to_string('msgs/paper_request.html', {'paper_request': paper}, RequestContext(request))
            message = Message.objects.create(
                sender=student.user,
                recipient=paper.editor.user,
                subject="Paper Request from %s - %s" % (paper.student.user.get_full_name(), paper.title),
                message=message_data,
                paper_request=paper
            )
            msg = SendEmail(request, file=[paper.raw_paper.path])
            msg.send(recipient=[paper.editor.user.email], template_path='email_messages/paper_request.html',
                context={
                    'paper_request': paper
                },
                subject='[EducationStudio] New paper received for editing!'
            )

            msg = SendEmail(request)
            msg.send(recipient=[paper.student.user.email], template_path='email_messages/paper_request_user.html',
                context={
                    'paper_request': paper,
                    'amount': amount/100.0,
                    'currency': settings.PLATFORM_CURRENCY_DISPLAY
                },
                subject='[EducationStudio] Your request has been submitted successfully!'
            )

            messages.add_message(request, messages.INFO, "Thanks {}, You have successfully made the payment of {} {}.".format(paper.student.user.get_full_name(), settings.PLATFORM_CURRENCY_DISPLAY, total_price))

            return redirect(reverse("profile", kwargs={"username": paper.editor.user.username}))
        else:
            messages.add_message(request, messages.ERROR, "You might have provided wrong information Please try again.")

    return render_to_response("checkout.html", {
        "current": "check_out",
        'paper': paper,
        'service_fee': '{:.2f}'.format(get_service_fee(paper.price)),
        'total_price': '{:.2f}'.format(total_price),
        'platform_fee': settings.PLATFORM_FEE,
        'currency': settings.PLATFORM_CURRENCY_DISPLAY,
        'publishable': settings.STRIPE_PUBLIC_KEY
    },  context_instance=RequestContext(request))


def get_context(editor):
    content_type = ContentType.objects.get_for_model(PaperRequest)
    papers = PaperRequest.objects.filter(editor=editor, status__in=['Ready To Review', 'Approved', 'Completed']).order_by('-created')

    score = 0
    has_rating = False

    paper_ids = papers.values_list('id', flat=True)
    ratings = Rating.objects.filter(content_type=content_type, object_id__in=paper_ids)
    for rating in ratings:
        score += rating.average

    if ratings:
        score = score/len(ratings)
        has_rating = True

    score = float(score)
    star = int(score)
    semi_star = not score.is_integer()
    star_empty = 4 - int(score) if semi_star else 5 - int(score)

    context = {
        "editor": editor,
        "star": range(star),
        "semi_star": semi_star,
        'star_empty': range(star_empty),
        "has_rating": has_rating
    }

    return context


@login_required(login_url=settings.LOGIN_URL)
def account_settings(request):
    editor = Editors.objects.get(user=request.user)

    try:
        stripe_profile = StripeProfile.objects.get(user=request.user)
    except StripeProfile.DoesNotExist:
        stripe_profile = None

    context = get_context(editor)

    context['publishable'] = settings.STRIPE_PUBLIC_KEY
    context['activities'] = FinancialActivity.objects.filter(editor__user=request.user).order_by('-created')
    context['currency'] = settings.PLATFORM_CURRENCY_DISPLAY
    context['stripe_profile'] = stripe_profile
    context["current"] = "settings"
    context["start"] = 10
    
    if len(context['activities']) > 10:
        context['activities'] = context['activities'][:10]
        context['more'] = True
    else:
        context['more'] = False

    return render_to_response("editors/settings.html", context, context_instance=RequestContext(request))


def load_more_fiancial_activities(request):
    start = int(request.GET.get('start'))

    editor = Editors.objects.get(user=request.user)

    context = get_context(editor)

    context['activities'] = FinancialActivity.objects.filter(editor__user=request.user).order_by('-created')

    if len(context['activities']) > start+10:
        context['activities'] = context['activities'][start:start+10]
        context['more'] = True
    else:
        context['activities'] = context['activities'][start:]
        context['more'] = False

    template = render_to_string("editors/load_more_fiancial_activities.html", context ,context_instance=RequestContext(request))

    response = {"start": start+10, "more": context['more'], "data": template}

    return HttpResponse(json.dumps(response), content_type="applicaton/json") 


@login_required(login_url=settings.LOGIN_URL)
def add_card(request):
    if request.method == 'POST':

        if 'stripeToken' in request.POST:
            token = request.POST['stripeToken']
        else:
            messages.error(request, "There was an issue, please try again.")
            return redirect(reverse("settings"))

        payment_method_obj = PaymentMethod(request.user)
        stripe_response = payment_method_obj.add(token)

        if stripe_response['status']:
            messages.add_message(request, messages.INFO, "You have successfully added your card")
            return redirect(reverse("settings"))
        else:
            messages.add_message(request, messages.ERROR, stripe_response['msg'])
            return redirect(reverse("settings"))

    return redirect(reverse("settings"))


@login_required(login_url=settings.LOGIN_URL)
def make_primary(request, payment_method_id):
    payment_method_obj = PaymentMethod(request.user, payment_method_id)
    stripe_response = payment_method_obj.make_primary()

    if not stripe_response['status'] and 'msg' in stripe_response:
        messages.add_message(request, messages.ERROR, stripe_response['msg'])
    else:
        messages.add_message(request, messages.INFO, "You have successfully update your card.")

    return redirect(reverse("settings"))


@login_required(login_url=settings.LOGIN_URL)
def delete_card(request, payment_method_id):
    payment_method_obj = PaymentMethod(request.user, payment_method_id)
    stripe_response = payment_method_obj.delete()

    if not stripe_response['status'] and 'msg' in stripe_response:
        messages.add_message(request, messages.ERROR, stripe_response['msg'])
    else:
        messages.add_message(request, messages.INFO, "You have successfully deleted your card from payment methods.")

    return redirect(reverse("settings"))


@login_required(login_url=settings.LOGIN_URL)
def get_paid(request):
    editor = Editors.objects.get(user=request.user)

    try:
        stripe_profile = StripeProfile.objects.get(user=request.user)
    except StripeProfile.DoesNotExist:
        messages.error(request, 'Please connect your stripe account to initiate the transfer!')
        return redirect(reverse("settings")+"?financial_activities_link=true")

    context = get_context(editor)

    form = GetPaidForm(request.POST or None, initial={'amount': editor.get_total_balance()}, user=request.user)

    if request.method == "POST":
        if form.is_valid():
            dollar_amount = form.cleaned_data['amount']
            cent_amount = int(dollar_amount * 100)

            stripe.api_key = settings.STRIPE_SECRET_KEY

            try:
                response = stripe.Transfer.create(
                    amount=cent_amount,
                    currency=settings.PLATFORM_CURRENCY,
                    destination=stripe_profile.stripe_id
                )
            except:
                messages.error(request, 'Transfer failed! Please contact support for the transfers')
                return redirect(reverse("settings"))

            activity = FinancialActivity.objects.create(
                editor=editor,
                description="Withdraw",
                type="Debit",
                amount=dollar_amount,
                transfer_id=response.id
            )

            msg = SendEmail(request)
            msg.send(recipient=[editor.user.email], template_path='email_messages/activity.html',
                context={
                    'activity': activity,
                    'editor': editor,
                    'currency': settings.PLATFORM_CURRENCY_DISPLAY,
                    'financial_activities_link': settings.SITE_URL + reverse('settings')
                },
                subject='[EducationStudio] Transfer Receipt!'
            )

            messages.add_message(request, messages.INFO, "You have successfully transferred {} {} to your account.".format(settings.PLATFORM_CURRENCY_DISPLAY, dollar_amount))

            return redirect(reverse("settings"))

    context["form"] = form
    context['currency'] = settings.PLATFORM_CURRENCY_DISPLAY

    return render_to_response("editors/get_paid.html", context, context_instance=RequestContext(request))


@login_required
@csrf_exempt
def update_editor_rating(request):
    paper_id = request.GET.get('paper_id', '')
    feedback = request.POST.get('feedback', '')

    paper = PaperRequest.objects.get(id=paper_id)

    content_type = ContentType.objects.get_for_model(PaperRequest)

    papers = PaperRequest.objects.filter(editor=paper.editor, status='Completed').order_by('-created')
    paper_ids = papers.values_list('id', flat=True)
    ratings = Rating.objects.filter(content_type=content_type, object_id__in=paper_ids)

    paper_rating = Rating.objects.get(content_type=content_type, object_id=paper_id)

    f, created = RatingFeedback.objects.get_or_create(rating=paper_rating)
    f.feedback = feedback
    f.save()

    score, star, semi_star, star_empty, has_rating = calculate_rating_stars(ratings=ratings)

    paper.editor.rating_star = score
    paper.editor.save()

    return HttpResponse(json.dumps({'status': True}))
