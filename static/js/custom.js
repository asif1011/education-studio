function save_qualificaton($this){
    $('.error-msgs').hide();
    $('.form-group').removeClass('has-error');

    var formdata = new FormData($('#id_qualifiction_form')[0]);
    $.ajax({
        url: $this.attr('action'),
        type: $this.attr('method'),
        processData: false,
        contentType: false,
        data: formdata,
        success: function(response){
            response = JSON.parse(response);
            if(response.status){
                $('#id_added_qualifications').append(response.qualification);
                $('#id_qualifiction_form').remove();
                $('#add_more_qualification').removeClass('disabled');
                $('#add_more_qualification').attr('onclick', 'add_qualificaton()');
            }else{
                for(var i=0; i<response.errors.length; i++){
                    if (response.errors[i].key == '__all__'){
                        $this.find('#id_all_error_qualification').text(response.errors[i].error);
                        $this.find('#id_all_error_qualification').show();
                    }else{
                        $this.find('#id_'+response.errors[i].key+'_error').text(response.errors[i].error);
                        $this.find('#id_'+response.errors[i].key+'_error').show();
                        $this.find('#id_'+response.errors[i].key+'_error_parent_div').addClass('has-error');
                    }
                }
            }
        }
    });
    return false;
}

function save_experience($this){
    $('.error-msgs').hide();
    $('.form-group').removeClass('has-error');
    $.ajax({
        url: $this.attr('action'),
        type: $this.attr('method'),
        data: $this.serialize(),
        success: function(response){
            response = JSON.parse(response);
            if(response.status){
                $('#id_added_experience').append(response.experience);
                $('#id_experience_form').remove();
                $('#add_more_experience').removeClass('disabled');
                $('#add_more_experience').attr('onclick', 'add_experience()');
            }else{
                for(var i=0; i<response.errors.length; i++){
                    if (response.errors[i].key == '__all__'){
                        $this.find('#id_all_error_qualification').text(response.errors[i].error);
                        $this.find('#id_all_error_qualification').show();
                    }else{
                        $this.find('#id_'+response.errors[i].key+'_error').text(response.errors[i].error);
                        $this.find('#id_'+response.errors[i].key+'_error').show();
                        $this.find('#id_'+response.errors[i].key+'_error_parent_div').addClass('has-error');
                    }
                }
            }
        }
    });
    return false;
}

function remove_qualification(id) {
    $.ajax({
        url: "/editors/show-qualification",
        type: "GET",
        data: {
            'qualification_id' : id,
        },
        success: function(response){
            response = JSON.parse(response);
            debugger;
            if (response.status) {
                $('#id_added_qualifications').append(response.qualification);
                $('#id_qualifiction_form').remove();
                $('#add_more_qualification').removeClass('disabled');
                $('#add_more_qualification').attr('onclick', 'add_qualificaton()');
            }
        }
    });
    return false;
}

function remove_experience(id) {
    $.ajax({
        url: "/editors/show-experience",
        type: "GET",
        data: {
            'experience_id' : id,
        },
        success: function(response){
            response = JSON.parse(response);
            if (response.status) {
                $('#id_added_experience').append(response.experience);
                $('#id_experience_form').remove();
                $('#add_more_experience').removeClass('disabled');
                $('#add_more_experience').attr('onclick', 'add_experience()');
            }
        }
    });
    return false;
}

function remove_qualification_form($this) {
    $this.closest("form").remove();
    $("#id_qualifiction_form").show();
    $('#add_more_qualification').removeClass('disabled');
    $('#add_more_qualification').attr('onclick', 'add_qualificaton()');
}

function remove_experience_form($this) {
    $this.closest("form").remove();
    $('#add_more_experience').removeClass('disabled');
    $('#add_more_experience').attr('onclick', 'add_experience()');
}

function delete_qualification_notify(qualification_id) {
    $("#id_delete_confirm").attr("onclick", "delete_qualification(" + qualification_id + ")");
    $("#id_delete_message").text("Do you really want to delete qualification from your account?");
    $("#id_delete_modal").modal("show");
}

function delete_experience_notify(experience_id) {
    $("#id_delete_confirm").attr("onclick", "delete_experience(" + experience_id + ")");
    $("#id_delete_message").text("Do you really want to delete experience from your account?");
    $("#id_delete_modal").modal("show");
}

function quote_request($this) {
    $('.error').hide();
    $('.form-group').removeClass('has-error');
    $('#id_quote_overlay').show();
    var formdata = new FormData($('#id_request_quote_form')[0]);

    $.ajax({
        url: $this.attr("action"),
        type: $this.attr("method"),
        processData: false,
        contentType: false,
        data: formdata,
        success: function(response) {
            response = JSON.parse(response);
            $('#id_quote_overlay').hide();

            if(response.status){
                location.reload();
            }else{
                if (response.errors.length == 0){
                    window.location = response.redirect_to;
                }
                for(var i=0; i<response.errors.length; i++){
                    if (response.errors[i].key == '__all__'){
                        $this.find('#id_all_error_qualification').text(response.errors[i].error);
                        $this.find('#id_all_error_qualification').show();
                    }else{
                        $this.find('#id_'+response.errors[i].key+'_error').text(response.errors[i].error);
                        $this.find('#id_'+response.errors[i].key+'_error').show();
                        $this.find('#id_'+response.errors[i].key+'_error_parent_div').addClass('has-error');
                    }
                }
            }
        }
    });
    return false;
}

function paper_request($this) {
    $('.error').hide();
    $('.form-group').removeClass('has-error');
    $('#id_paper_overlay').show();

    var formdata = new FormData($('#id_paper_form')[0]);

    $.ajax({
        url: $this.attr("action"),
        type: $this.attr("method"),
        processData: false,
        contentType: false,
        data: formdata,
        success: function(response) {
            response = JSON.parse(response);

            $('#id_paper_overlay').hide();

            if(response.status){
                window.location = response.next;
            }else{
                if (response.errors.length == 0){
                    window.location = response.redirect_to;
                }
                for(var i=0; i<response.errors.length; i++){
                    if (response.errors[i].key == '__all__'){
                        $this.find('#id_all_error_qualification').text(response.errors[i].error);
                        $this.find('#id_all_error_qualification').show();
                    }else{
                        $this.find('#id_'+response.errors[i].key+'_error').text(response.errors[i].error);
                        $this.find('#id_'+response.errors[i].key+'_error').show();
                        $this.find('#id_'+response.errors[i].key+'_error_parent_div').addClass('has-error');
                    }
                }
            }
        }
    });
    return false;
}

function reject_paper($this, paper_id){

    $('#accpet').addClass('disabled');
    $("#decline").addClass('disabled');
    $("#updating").show();
    $.ajax({
        url: "/editors/paper-reject/"+paper_id+"/update-status",
        type: "GET",
        success :function(response){
            debugger;
            response = JSON.parse(response);
            if (response.status){
                $this.text("Paper Rejected")
                $("#updating").hide();
                $("#id_paper_status_"+paper_id).text("Rejected");
                $('#id_message_alert').html(response.message);
                $('#id_message_alert').show();
                $('.message-alert').fadeOut(6000);
                location.reload(true);
            }
        }
    });
    return false;
}

function change_paper_status($this, current_status, paper_id) {
    if (current_status == "Pending") {
        $('#accpet').addClass('disabled');
        $("#decline").addClass('disabled');
        $('#updating').show();
        $.ajax({
            url: "/editors/paper/"+paper_id+"/update-status",
            type: "GET",
            success: function(response){
                response = JSON.parse(response);
                if (response.status) {
                    $this.text("Send for review");
                    $("#accpet").show();
                    $('#updating').hide();
                    $("#id_paper_status_"+paper_id).text("Progress");
                    $this.attr("onclick", "change_paper_status($(this), 'Progress', "+paper_id+")");
                    $('#id_message_alert').html(response.message);
                    $('#id_message_alert').show();
                    $('.message-alert').fadeOut(6000);
                    location.reload(true);
                }
            }
        })
    } else if (current_status == "Progress" || current_status== "Ready To Review") {
        $("#review").show();
        $("#id_paper_id").val(paper_id);
        $("#id_paper_complete_modal").modal("show");
    }
}

function complete_paper_form($this) {
    $('.error').hide();
    $('.form-group').removeClass('has-error');
    $('#id_paper_complete_overlay').show();

    var formdata = new FormData($('#id_paper_complete_form')[0]);
    $.ajax({
        url: $this.attr("action"),
        type: $this.attr("method"),
        processData: false,
        contentType: false,
        data: formdata,
        success: function(response) {
            debugger;

            response = JSON.parse(response);

            $('#id_paper_complete_overlay').hide();

            if(response.status){
                window.location = response.next;
            }else{
                for(var i=0; i<response.errors.length; i++){
                    if (response.errors[i].key == '__all__'){
                        $this.find('#id_all_error_qualification').text(response.errors[i].error);
                        $this.find('#id_all_error_qualification').show();
                    }else{
                        $this.find('#id_'+response.errors[i].key+'_error').text(response.errors[i].error);
                        $this.find('#id_'+response.errors[i].key+'_error').show();
                        $this.find('#id_'+response.errors[i].key+'_error_parent_div').addClass('has-error');
                    }
                }
            }
        }
    });
    return false;
}

function add_to_cart_form(paper_id) {
    $("#id_add_to_cart_modal").find("#id_paper_id").val(paper_id);
    $("#id_add_to_cart_modal").modal("show");
}

function add_to_cart($this) {
    $('.error').hide();
    $('.form-group').removeClass('has-error');
    $('#id_add_to_cart_overlay').show();

    $.ajax({
        url: $this.attr("action"),
        type: $this.attr("method"),
        data: $this.serialize(),
        success: function(response) {
            response = JSON.parse(response);

            $('#id_add_to_cart_overlay').hide();

            if(response.status){
                $("#id_add_to_cart_modal").modal("hide");
                $this[0].reset()
                window.location = response.next
            }else{
                for(var i=0; i<response.errors.length; i++){
                    if (response.errors[i].key == '__all__'){
                        $this.find('#id_all_error_qualification').text(response.errors[i].error);
                        $this.find('#id_all_error_qualification').show();
                    }else{
                        $this.find('#id_'+response.errors[i].key+'_error').text(response.errors[i].error);
                        $this.find('#id_'+response.errors[i].key+'_error').show();
                        $this.find('#id_'+response.errors[i].key+'_error_parent_div').addClass('has-error');
                    }
                }
            }
        }
    });
    return false;
}

function remove_error() {
    $('.es-error').hide();
    $('.form-group').removeClass('has-error');
    $('.reset').val('');
    $("#id_paper_complete_modal").modal("hide");
    $("#id_paper_price_modal").modal("hide");
    $("#id_add_to_cart_modal").modal("hide");
    $("#id_rating_modal").modal("hide");
}


function add_qualificaton(qualification_id){
    if (qualification_id != "undefined") {
        $("#id_qualification_"+qualification_id).remove();
    }
    $.ajax({
        url: Window.base.urls.get_qualification_form,
        type: "GET",
        data: {'qualification_id': qualification_id},
        success: function(response){
            response = JSON.parse(response);
            if(response.status){
                $('#add_more_qualification').addClass('disabled');
                $('#add_more_qualification').removeAttr('onclick');
                $("#id_added_qualifications").html(response.data);
                select2_dropdown_ui()
            }
        }
    });
    return false;
}

function add_experience(experience_id){
    if (experience_id != "undefined") {
        $("#id_experience_"+experience_id).remove();
    }
    $.ajax({
        url: Window.base.urls.get_experience_form,
        type: "GET",
        data: {'experience_id': experience_id},
        success: function(response){
            response = JSON.parse(response);
            if(response.status){
                $('#add_more_experience').addClass('disabled');
                $('#add_more_experience').removeAttr('onclick');
                $("#id_added_experience").html(response.data);
                select2_dropdown_ui();
                if($('input[name=is_current]:checked').length > 0){
                   $("#id_to_experience").hide();
                }else{
                    $("#id_to_experience").show();
                }
                $("input[name=is_current]").change(function(){
                    if($('input[name=is_current]:checked').length > 0){
                       $("#id_to_experience").hide();
                    }else{
                        $("#id_to_experience").show();
                    }
                });
            }
        }
    });
    return false;
}

function delete_qualification(qualification_id){
    $.ajax({
        url: Window.base.urls.delete_qualification+"?qualification_id="+qualification_id,
        type: "GET",
        success: function(response){
            response = JSON.parse(response);
            if(response.status){
                $("#id_delete_modal").modal("hide");
                $( "#id_qualification_"+qualification_id).remove();
            }
        }
    });
    return false;
}

function delete_experience(experience_id){
    $.ajax({
        url: Window.base.urls.delete_experience+"?experience_id="+experience_id,
        type: "GET",
        success: function(response){
            response = JSON.parse(response);
            if(response.status){
                $("#id_delete_modal").modal("hide");
                $( "#id_experience_"+experience_id).remove();
            }
        }
    });
    return false;
}

function delete_notify(type) {
    $("#id_delete_confirm").attr("onclick", "delete_file('" + type + "')");
    $("#id_delete_message").text("Do you really want to delete " + type.replace("_", " ") + " from your account?");
    $("#id_delete_modal").modal("show");
}

function delete_file(type) {
    $.ajax({
        url: Window.base.urls.delete_file+"?type="+type,
        type: "GET",
        success: function(response){
            response = JSON.parse(response);
            if(response.status){
                $("#id_delete_modal").modal("hide");
                $("#id_display_"+type).remove();
            }
        }
    });
    return false;
}

function select2_dropdown_ui(){
    plugins.selectFilter = $("select");
    if (plugins.selectFilter.length) {
        var i;
        for (i = 0; i < plugins.selectFilter.length; i++) {
            var select = $(plugins.selectFilter[i]);
            select.select2({
                theme: "bootstrap"
            }).next().addClass(select.attr("class").match(/(input-sm)|(input-lg)|($)/i).toString().replace(new RegExp(",", 'g'), " "));
        }
    }
}

function openmodal(id) {
    $.ajax({
        url: "/student/open-paper-modal/",
        type: "GET",
        data: {
            'editor_id' : id,
        },
        success: function(response){
            response = JSON.parse(response);
            if (response.status) {
                
                $('body').append(response.data);

                $("#id_paper_modal").on('show.bs.modal', function(e) {
                    $(".rd-navbar").css("z-index", "1000");
                });

                $("#id_paper_modal").on('hide.bs.modal', function(e) {
                    $(".rd-navbar").css("z-index", "999999");
                });

                $("#id_paper_modal").modal("show");
                select2_dropdown_ui()

            }
        }
    });
    return false;
}