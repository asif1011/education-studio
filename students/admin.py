from django.contrib import admin

from .models import Students, StudentFinancialActivity


class StudentsAdmin(admin.ModelAdmin):
    list_display = ['user', 'nationality', 'country']
    search_fields = ['user__first_name', 'user__last_name']

class StudentFinancialActivityAdmin(admin.ModelAdmin):
    list_display = ['student', 'paper', 'description', 'amount', 'type', 'created']
    search_fields = ['student__user__first_name', 'student__user__last_name', 'paper__title', 'description', 'amount', 'type', 'created']    


admin.site.register(StudentFinancialActivity, StudentFinancialActivityAdmin)
admin.site.register(Students, StudentsAdmin)
