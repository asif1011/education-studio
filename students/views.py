import json, os
import random
import json
import requests

from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from eseditor.utils import SendEmail
from django.template.loader import render_to_string

from editors.utils import create_username, get_charge_price, get_transfer_price, get_service_fee
from students.models import Students, StudentFinancialActivity
from students.forms import EditStudentForm
from editors.models import PaperRequest, Quotation, Editors, Discipline, ProfileViewer
from django.core.files.base import ContentFile
from editors.forms import ShortPaperForm


def create_student_profile(email, first_name, last_name):
    user = User.objects.create(
        username=create_username('%s %s' % (first_name, last_name)),
        first_name=first_name,
        last_name=last_name,
        email=email,
        is_active=True
    )

    string = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()?"
    password = "".join(random.sample(string, 8))

    user.set_password(password)
    user.save()

    student = Students.objects.create(
        user=user
    )

    msg = SendEmail()
    msg.send(recipient=[student.user.email], template_path='email_messages/new_student_application.html',
        context={
            'student': student,
            'password' : password
        },
        subject='Welcome to EducationStudio!'
    )

    return student


def openmodal(request):
    if request.is_ajax():
        editor_id = request.GET.get('editor_id', '')
        editor = Editors.objects.get(id=editor_id)

        if editor:
            paper_form = ShortPaperForm(initial={"editor_id": editor.id})

        paper_form_content = render_to_string('students/paper_form.html', {
            'paper_form': paper_form,
            'editor' : editor,
        }, context_instance=RequestContext(request))

        response = {
            "data": paper_form_content,
            "status": True
        }
        return HttpResponse(json.dumps(response))
    else:
        return Http404


@login_required
def student_profile(request, username):
    """
    function is used to provide the profile page view of a student
    """
    student = Students.objects.get(user__username=username)
    if not student.user == request.user:
        return redirect('/')

    papers = PaperRequest.objects.filter(student=student).exclude(status='Draft').order_by('-created')

    quotes = Quotation.objects.filter(student=student).order_by('-created')

    if request.user.is_authenticated():    
        save_profile_viewer = ProfileViewer.objects.filter(user=request.user).order_by('-modified')[:10]
    else:
        save_profile_viewer = []

    if len(papers) > 10:
        papers = papers[:10]
        more = True
    else:
        more = False

    if len(quotes) > 10:
        quotes = quotes[:10]
        more1 = True
    else:
        more1 = False   

    return render_to_response("students/student_profile.html", {
        "current": "student_profile",
        "save_profile_viewer": save_profile_viewer,
        'paper_form' : ShortPaperForm(),
        "start": 10,
        "more": more,
        "start1": 10,
        "more1": more1,
        "student": student,
        "papers" : papers,
        "quotes" : quotes,
        "currency": settings.PLATFORM_CURRENCY_DISPLAY
    }, context_instance=RequestContext(request))


def load_more_paper(request):
    start = int(request.GET.get('start'))

    student = Students.objects.get(user=request.user)

    papers = PaperRequest.objects.filter(student=student).exclude(status='Draft').order_by('-created')
    
    if len(papers) > start+10:
        papers = papers[start:start+10]
        more = True
    else:
        papers = papers[start:]
        more = False

    context = {"papers": papers, "start":start}

    template = render_to_string("students/load_more_paper.html", context ,context_instance=RequestContext(request))

    response = {"start": start+10, "more": more, "data": template}

    return HttpResponse(json.dumps(response), content_type="applicaton/json")


def load_more_quotes(request):
    start1 = int(request.GET.get('start1'))

    student = Students.objects.get(user=request.user)

    quotes = Quotation.objects.filter(student=student).order_by('-created')
    
    if len(quotes) > start1+10:
        quotes = quotes[start1:start1+10]
        more1 = True
    else:
        quotes = quotes[start1:]
        more1 = False

    context = {"quotes": quotes, "start":start1}

    template = render_to_string("students/load_more_quotes.html", context ,context_instance=RequestContext(request))

    response = {"start1": start1+10, "more1": more1, "data": template}

    return HttpResponse(json.dumps(response), content_type="applicaton/json")    


@login_required
def edit_student(request):
    """
    function is used to edit the student profile.
    """
    student= Students.objects.get(user=request.user)

    if request.method == "POST":
        form = EditStudentForm(request.POST, request.FILES, instance=student)

        if form.is_valid():
            obj = form.save(commit=False)
            obj.user.first_name = form.cleaned_data["first_name"]
            obj.user.last_name = form.cleaned_data["last_name"]
            obj.user.email = form.cleaned_data["email"]
            obj.user.save()
            obj.save()

            default_avatar = form.cleaned_data["default_avatar"]

            if default_avatar:
                default_avatar = default_avatar[1:]
                avatar_path = os.path.join(settings.BASE_DIR, default_avatar)
                filename = default_avatar.split("/")[-1]
                with open(avatar_path, 'r') as f:
                    student.avatar.save(filename, ContentFile(f.read()))
                f.close()

            return redirect(reverse("student_profile", kwargs={"username": student.user.username}))

        return render_to_response("students/edit_student.html",{
            "form": form,
        }, context_instance=RequestContext(request))

    else:
        form = EditStudentForm(instance=student, initial={
            'first_name': student.user.first_name,
            'last_name': student.user.last_name,
            'email': student.user.email
        })

        return render_to_response("students/edit_student.html",{
            "current": "edit",
            "form": form,
            "student": student
        }, context_instance=RequestContext(request))



@login_required(login_url=settings.LOGIN_URL)
def student_settings(request):
    """
    Function to show financial Activities of Student
    """
    student = Students.objects.get(user=request.user)
    papers = PaperRequest.objects.filter(student=student, status__in=['Ready To Review', 'Approved', 'Completed']).order_by('-created')
    activities = StudentFinancialActivity.objects.filter(student__user=request.user).order_by('-created')

    if len(activities) > 10:
        activities = activities[:10]
        more = True
    else:
        more = False

    return render_to_response("students/settings.html", {
        "start": 10,
        "more": more,
        "current": "settings",
        "student": student,
        "papers" :papers,
        "activities" :activities,
        "currency": settings.PLATFORM_CURRENCY_DISPLAY
    }, context_instance=RequestContext(request))    


def load_more_activities(request):
    start = int(request.GET.get('start'))

    student = Students.objects.get(user=request.user)

    activities = StudentFinancialActivity.objects.filter(student__user=request.user).order_by('-created')
    
    if len(activities) > start+10:
        activities = activities[start:start+10]
        more = True
    else:
        activities = activities[start:]
        more = False

    context = {"activities": activities, "start":start}

    template = render_to_string("students/load_more_activities.html", context ,context_instance=RequestContext(request))

    response = {"start": start+10, "more": more, "data": template}

    return HttpResponse(json.dumps(response), content_type="applicaton/json")