from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from editors.models import PaperRequest


class Message(models.Model):
    sender = models.ForeignKey(User, related_name="sender_user")
    recipient = models.ForeignKey(User, related_name="recipient_user")
    sender_read = models.BooleanField(default=False)
    paper_request = models.ForeignKey(PaperRequest, null=True, blank=True)
    recipient_read = models.BooleanField(default=False)
    parent_msg = models.ForeignKey("self", null=True, blank=True, related_name="parent")
    subject  = models.CharField(max_length=255, blank=True, null=True)
    message = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return u'%s' % self.subject

    def get_last_child(self):
        if self.parent.all():
            last_child = self.parent.all().order_by("-created")[0]
        else:
            last_child = self
        return last_child
