from django.conf import settings
from message.models import Message


def get_message_counter(request):
    if request.user.is_authenticated():
        new_msg = Message.objects.filter(recipient = request.user).filter(recipient_read=False).order_by('-created')
        msg_counter = len(new_msg)
        return {'msg_counter': msg_counter}
    else:
        return {}
